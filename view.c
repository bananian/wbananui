/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <xkbcommon/xkbcommon.h>
#include <bananui/utils.h>
#include <bananui/window.h>
#include <bananui/view.h>

#define SCROLL_STEP 20.0

static bWidgetListItem *find_prev_focusable(bBoxWidget *top,
	bWidgetListItem *cur)
{
	bWidgetListItem *tmp;

	if(!top->children.last) return NULL;

	if(!cur){
		cur = top->children.last;
		if(cur && cur->type == B_WIDGET_TYPE_BOX &&
			cur->box->focusable)
		{
			return cur;
		}
	}

	tmp = cur;
	do {
		if(tmp->type == B_WIDGET_TYPE_BOX && tmp->box->children.last){
			tmp = tmp->box->children.last;
		}
		else if(tmp->prev){
			tmp = tmp->prev;
		}
		else {
			do {
				tmp = tmp->owner->self;
			} while(tmp && tmp != top->self && !tmp->prev);

			if(tmp && tmp != top->self && tmp->prev){
				tmp = tmp->prev;
			} else {
				tmp = top->children.last;
			}
		}
	} while(tmp && tmp != cur &&
		(tmp->type != B_WIDGET_TYPE_BOX || !tmp->box->focusable));

	return (tmp && tmp->type == B_WIDGET_TYPE_BOX && tmp->box->focusable) ?
		tmp : NULL;
}

static bWidgetListItem *find_next_focusable(bBoxWidget *top,
	bWidgetListItem *cur)
{
	bWidgetListItem *tmp;

	if(!top->children.first) return NULL;

	if(!cur){
		cur = top->children.first;
		if(cur && cur->type == B_WIDGET_TYPE_BOX &&
			cur->box->focusable)
		{
			return cur;
		}
	}

	tmp = cur;
	do {
		if(tmp->type == B_WIDGET_TYPE_BOX && tmp->box->children.first){
			tmp = tmp->box->children.first;
		}
		else if(tmp->next){
			tmp = tmp->next;
		}
		else {
			do {
				tmp = tmp->owner->self;
			} while(tmp && tmp != top->self && !tmp->next);

			if(tmp && tmp != top->self && tmp->next){
				tmp = tmp->next;
			} else {
				tmp = top->children.first;
			}
		}
	} while(tmp && tmp != cur &&
		(tmp->type != B_WIDGET_TYPE_BOX || !tmp->box->focusable));

	return (tmp && tmp->type == B_WIDGET_TYPE_BOX && tmp->box->focusable) ?
		tmp : NULL;
}

static int focus_destroy_handler(void *param, void *data)
{
	bView *view = data;
	/*
	 * Reset focus
	 * TODO: we could keep the scroll offset here and make the render
	 * handler find a closer focused widget
	 */
	view->selected = NULL;
	view->sy = 0;
	view->sx = 0;
	return 1;
}

static void view_go_up(bView *view, bWidgetListItem *prev)
{
	int prev_is_under_current = prev && view->selected &&
		view->selected->box->computed.top <=
		prev->box->computed.top;
	double full_scroll = view->body->computed.height - view->height;
	double prev_top = 0;
	if(prev) prev_top = prev->box->computed.top - view->header_height;

#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Focus will be on: %p\n", (void*)prev);
#endif
	if(view->rendered && view->scrollable){
		if((!prev || prev_top <= -view->height/2 ||
			prev_is_under_current) && full_scroll > 0)
		{
			view->sy -= SCROLL_STEP;
			if(view->sy < 0){
				if(prev_is_under_current &&
					view->sy <= -SCROLL_STEP)
				{
					view->sy += SCROLL_STEP;
				}
				else {
					view->sy = 0;
					return;
				}
			}
			else return;
		}
		if(prev_is_under_current && prev_top > view->height){
			view->sy += prev_top;
			if(view->sy > full_scroll) view->sy = full_scroll;
		}
		else if(prev && prev_top < 0){
			view->sy += prev_top;
		}
	}
	if(view->selected){
		bTriggerEvent(view->selected->box->focusout, NULL);
		bUnregisterEventHandler(&view->selected->box->destroy,
				focus_destroy_handler, view);
	}
	view->selected = prev;
	if(view->selected){
		bRegisterEventHandler(&view->selected->box->destroy,
				focus_destroy_handler, view);
		bTriggerEvent(view->selected->box->focusin, NULL);
	}
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Focus applied\n");
#endif
}

static void view_go_down(bView *view, bWidgetListItem *next)
{
	int next_is_above_current = next && view->selected &&
		view->selected->box->computed.top >=
		next->box->computed.top;
	double next_top = 0;
	if(next){
		next_top = next->box->computed.top - view->header_height;
	}

#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Focus will be on: %p\n", (void*)next);
#endif
	if(view->rendered && view->scrollable){
		double full_scroll = view->body->computed.height -
			view->height;
		if((!next || next_top >= view->height*3/2 ||
			next_is_above_current) && full_scroll > 0)
		{
			view->sy += SCROLL_STEP;
			if(view->sy > full_scroll){
				if(next_is_above_current &&
					view->sy >= (full_scroll + SCROLL_STEP))
				{
					view->sy -= SCROLL_STEP;
				}
				else {
					view->sy = full_scroll;
					return;
				}
			}
			else return;
		}
		if(next_is_above_current && next_top < 0){
			view->sy += next_top +
				next->box->computed.height - view->height;
			if(view->sy < 0) view->sy = 0;
		}
		else if(next && (next_top + next->box->computed.height) >
			view->height)
		{
			view->sy += next_top +
				next->box->computed.height - view->height;
		}
	}
	if(view->selected){
		bTriggerEvent(view->selected->box->focusout, NULL);
		bUnregisterEventHandler(&view->selected->box->destroy,
				focus_destroy_handler, view);
	}
	view->selected = next;
	if(view->selected){
		bRegisterEventHandler(&view->selected->box->destroy,
				focus_destroy_handler, view);
		bTriggerEvent(view->selected->box->focusin, NULL);
	}
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Focus applied\n");
#endif
}

static int view_handle_key(void *param, void *data)
{
	bView *view = data;
	xkb_keysym_t key = *((xkb_keysym_t*)param);
	if(!bTriggerEvent(view->keydown, param)) return 0;
	switch(key){
		case XKB_KEY_Up:
			view_go_up(view, find_prev_focusable(view->body,
						view->selected));
			if(view->cur_wnd) bRedrawWindow(view->cur_wnd);
			break;
		case XKB_KEY_Down:
			view_go_down(view, find_next_focusable(view->body,
						view->selected));
			if(view->cur_wnd) bRedrawWindow(view->cur_wnd);
			break;
		case XKB_KEY_Return:
			if(view->selected){
				return bTriggerEvent(
						view->selected->box->click,
						NULL);
			}
			break;
	}
	return 1;
}

static int view_handle_keyup(void *param, void *data)
{
	bView *view = data;
	return bTriggerEvent(view->keyup, param);
}

static int view_render(void *param, void *data)
{
	bWindow *wnd = param;
	bView *view = data;

	if(!view->selected){
		view->selected = find_next_focusable(view->body, NULL);
#if BANANUI_VERBOSE
		fprintf(stderr, "[DEBUG] First focus: %p\n",
				(void*)view->selected);
#endif
		if(view->selected){
			bRegisterEventHandler(&view->selected->box->destroy,
					focus_destroy_handler, view);
			bTriggerEvent(view->selected->box->focusin, NULL);
		}
	}

	bShowMainBox(wnd->surf, wnd->cr, view->bg, 0, 0);
	if(view->header){
		bShowMainBox(wnd->surf, wnd->cr, view->body,
			-view->sy + view->header->computed.height, -view->sx);
		bShowMainBox(wnd->surf, wnd->cr, view->header, 0, 0);
	}
	else {
		bShowMainBox(wnd->surf, wnd->cr, view->body,
			-view->sy, -view->sx);
	}
	if(view->sk) bShowSoftkeyPanel(wnd->surf, wnd->cr, view->sk);
	view->rendered = 1;
	return 1;
}

void bShowView(bWindow *wnd, bView *view)
{
	if(view->header){
		bComputeLayout(wnd->surf, wnd->cr, view->header);
		view->header_height = view->header->computed.height;
	}
	else {
		view->header_height = 0;
	}
	view->cur_wnd = wnd;
	view->height = auto_surface_get_height(wnd->surf) -
		view->header_height - (view->sk ? SOFTKEY_HEIGHT : 0);
	view->rendered = 0;
	if(!view->scrollable) view->body->height = view->height;
	bRegisterEventHandler(&wnd->keydown, view_handle_key, view);
	bRegisterEventHandler(&wnd->keyup, view_handle_keyup, view);
	bRegisterEventHandler(&wnd->redraw, view_render, view);
	if(view->selected)
		bTriggerEvent(view->selected->box->focusin, NULL);
}

void bHideView(bWindow *wnd, bView *view)
{
	bUnregisterEventHandler(&wnd->keydown, view_handle_key, view);
	bUnregisterEventHandler(&wnd->keyup, view_handle_keyup, view);
	bUnregisterEventHandler(&wnd->redraw, view_render, view);
	if(view->selected)
		bTriggerEvent(view->selected->box->focusout, NULL);
}

void bSetFocus(bView *view, bBoxWidget *box, int align_at_bottom)
{
	if(!box->self) return;
	if(align_at_bottom)
		view_go_down(view, box->self);
	else
		view_go_up(view, box->self);
}

bView *bCreateView(int scrollable)
{
	bView *view;

	view = malloc(sizeof(bView));
	if(!view) return NULL;

	view->keyup = NULL;
	view->keydown = NULL;
	view->body = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 0);
	view->bg = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, -1);
	view->header = NULL;
	view->sk = NULL;
	view->cur_wnd = NULL;
	view->selected = NULL;
	view->scrollable = scrollable;

	view->sx = view->sy = 0;
	view->height = -1;
	view->rendered = 0;

	return view;
}
void bDestroyView(bView *view)
{
	if(!view) return;
	/* everything else should be freed by the user! */
	bDestroyBoxRecursive(view->body);
	bDestroyBoxRecursive(view->bg);
	free(view);
}
