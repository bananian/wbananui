/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2022 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <bananui/menu.h>

bMenu *bCreateMenuView(bWindow *wnd)
{
	bMenu *menu;
	menu = calloc(1, sizeof(bMenu));
	if(!menu) return NULL;
	menu->wnd = wnd;
	return menu;
}

void bSetMenuTitle(bMenu *m, const char *title)
{
	bContentWidget *txt;
	bView *view = m->current->view;
	bHideView(m->wnd, view);
	if(view->header) bDestroyBoxRecursive(view->header);
	view->header = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX, -1, 25);
	if(!view->header) return;
	txt = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!txt) return;
	view->header->color = bColorFromTheme("header:background");
	txt->color = bColorFromTheme("header:text");
	bSetTextContent(txt, title, PANGO_WEIGHT_BOLD, 17);
	bAddContentToBox(view->header, txt);
	/* recompute offset */
	bShowView(m->wnd, view);
}

int bPopMenu(bMenu *m)
{
	bMenuView *tmp = m->current;
	m->current = tmp->prev;
	bHideView(m->wnd, tmp->view);
	if(tmp->view->sk) bDestroySoftkeyPanel(tmp->view->sk);
	if(tmp->view->header) bDestroyBoxRecursive(tmp->view->header);
	bDestroyView(tmp->view);
	if(m->current) bShowView(m->wnd, m->current->view);
	bRedrawWindow(m->wnd);
	free(tmp);
	return m->current ? 1 : 0;
}

struct menu_click {
	bMenuEntry *e;
	bMenu *m;
};

static int handle_destroy(void *param, void *data)
{
	struct menu_click *mc = data;
	if(mc->e->button_handler) mc->e->button_handler(mc->m, NULL, mc->e->arg);
	free(mc);
	return 1;
}

static int handle_click(void *param, void *data)
{
	struct menu_click *mc = data;
	if(!mc->e->click_handler) return 1;
	return mc->e->click_handler(mc->m, mc->e->arg);
}

int bOpenMenuPrivate(bMenu *m, bMenuDef *def)
{
	int i;
	bView *view;
	bMenuView *mv;

	mv = calloc(1, sizeof(bMenuView));
	if(!mv) return -1;

	view = bCreateView(1);
	if(!view){
		free(mv);
		return -1;
	}

	mv->view = view;

	view->bg->color = bColorFromTheme("background");

	view->sk = bCreateSoftkeyPanel();
	if(view->sk) bSetSoftkeyText(view->sk, "", "SELECT", "");

	for(i = 0; i < def->num_entries; i++){
		struct menu_click *click;
		bButtonWidget *btn;
		bMenuEntry *ent = &def->entries[i];

		click = calloc(1, sizeof(struct menu_click));
		if(!click) continue;

		click->e = ent;
		click->m = m;

		btn = bCreateButtonWidget(ent->name, ent->style, B_ALIGN_START, -1,
				(ent->height > 60) ? ent->height : 60);
		if(!btn){
			free(click);
			continue;
		}
		if(ent->icon){
			bLoadImageFromIcon(btn->icon, ent->icon,
					ent->iconsize ? ent->iconsize : 24);
		}
		bRegisterEventHandler(&btn->box->click, handle_click, click);
		bRegisterEventHandler(&btn->box->destroy, handle_destroy, click);
		bAddBoxToBox(view->body, btn->box);
		if(ent->button_handler) ent->button_handler(m, btn, ent->arg);
	}

	if(m->current) bHideView(m->wnd, m->current->view);
	mv->prev = m->current;
	m->current = mv;
	bShowView(m->wnd, view);
	def->init(m);
	bRedrawWindow(m->wnd);
	return 0;
}
