#include <bananui/simple.h>

bWidgetColor
	sbDefaultBgColor = B_RGBA_INITIALIZER(1.0, 1.0, 1.0, 1.0),
	sbDefaultFgColor = B_RGBA_INITIALIZER(0.0, 0.0, 0.0, 1.0);

struct sbItem sbRGBA(double r, double g, double b, double a)
{
	struct sbItem ret = {
		.type = SB_ITEM_COLOR,
		.color = B_RGBA_INITIALIZER(r, g, b, a)
	};
	return ret;
}

struct sbItem sbMGNL(double mgn)
{
	struct sbItem ret = {
		.type = SB_ITEM_MGN_L,
		.margin = mgn
	};
	return ret;
}

struct sbItem sbMGNR(double mgn)
{
	struct sbItem ret = {
		.type = SB_ITEM_MGN_R,
		.margin = mgn
	};
	return ret;
}

struct sbItem sbMGNT(double mgn)
{
	struct sbItem ret = {
		.type = SB_ITEM_MGN_T,
		.margin = mgn
	};
	return ret;
}

struct sbItem sbMGNB(double mgn)
{
	struct sbItem ret = {
		.type = SB_ITEM_MGN_B,
		.margin = mgn
	};
	return ret;
}

struct sbItem sbREF(struct sbItem *ref){
	struct sbItem ret = {
		.type = SB_ITEM_REF,
		.ref = ref
	};
	return ret;
}

struct sbItem sbBOX(bContentAlign align, bBoxDirection dir,
	double width, double height, ...)
{
	va_list args;
	int colorSet = 0;
	struct sbItem sbi, ret = {
		.type = SB_ITEM_BOX,
		.box = bCreateBoxWidget(align, dir, width, height)
	};
	if(!ret.box){
		ret.err = "Out of memory";
		goto err;
	}
	va_start(args, height);
	while((sbi = va_arg(args, struct sbItem), sbi.type != SB_ITEM_END)){
		if(sbi.type == SB_ITEM_BOX)
			bAddBoxToBox(ret.box, sbi.box);
		else if(sbi.type == SB_ITEM_CONTENT)
			bAddContentToBox(ret.box, sbi.cont);
		else if(sbi.type == SB_ITEM_REF)
			*(sbi.ref) = ret;
		else if(sbi.type == SB_ITEM_MGN_L)
			ret.box->mgn_l = sbi.margin;
		else if(sbi.type == SB_ITEM_MGN_R)
			ret.box->mgn_r = sbi.margin;
		else if(sbi.type == SB_ITEM_MGN_T)
			ret.box->mgn_t = sbi.margin;
		else if(sbi.type == SB_ITEM_MGN_B)
			ret.box->mgn_b = sbi.margin;
		else if(sbi.type == SB_ITEM_COLOR){
			colorSet = 1;
			ret.box->color = sbi.color;
		}
		else {
			if(sbi.type == SB_ITEM_ERROR)
				ret.err = sbi.err;
			else
				ret.err = "Unrecognized child type";
			goto err;
		}
	}
	va_end(args);
	if(!colorSet) ret.box->color = sbDefaultBgColor;
	return ret;
err:
	if(ret.box) bDestroyBoxRecursive(ret.box);
	ret.type = SB_ITEM_ERROR;
	return ret;
}

struct sbItem sbTEXT(bContentAlign align, const char *txt,
	PangoWeight wt, double size, ...)
{
	va_list args;
	int colorSet = 0;
	struct sbItem sbi, ret = {
		.type = SB_ITEM_CONTENT,
		.cont = bCreateContentWidget(align, 0)
	};
	if(!ret.cont){
		ret.type = SB_ITEM_ERROR;
		ret.err = "Out of memory";
		return ret;
	}
	bSetTextContent(ret.cont, txt, wt, size);
	va_start(args, size);
	while((sbi = va_arg(args, struct sbItem), sbi.type != SB_ITEM_END)){
		if(sbi.type == SB_ITEM_REF)
			*(sbi.ref) = ret;
		else if(sbi.type == SB_ITEM_COLOR){
			colorSet = 1;
			ret.cont->color = sbi.color;
		}
	}
	if(!colorSet) ret.cont->color = sbDefaultFgColor;
	return ret;
}

struct sbItem sbIMAGE(bContentAlign align, const char *filename, ...)
{
	va_list args;
	struct sbItem sbi, ret = {
		.type = SB_ITEM_CONTENT,
		.cont = bCreateContentWidget(align, 0)
	};
	if(!ret.cont){
		ret.type = SB_ITEM_ERROR;
		ret.err = "Out of memory";
		return ret;
	}
	bLoadImageFromFile(ret.cont, filename);
	va_start(args, filename);
	while((sbi = va_arg(args, struct sbItem), sbi.type != SB_ITEM_END)){
		if(sbi.type == SB_ITEM_REF)
			*(sbi.ref) = ret;
	}
	return ret;
}

struct sbItem sbICON(bContentAlign align, const char *name,
	int size, ...)
{
	va_list args;
	int colorSet = 0;
	struct sbItem sbi, ret = {
		.type = SB_ITEM_CONTENT,
		.cont = bCreateContentWidget(align, 0)
	};
	if(!ret.cont){
		ret.type = SB_ITEM_ERROR;
		ret.err = "Out of memory";
		return ret;
	}
	bLoadImageFromIcon(ret.cont, name, size);
	va_start(args, size);
	while((sbi = va_arg(args, struct sbItem), sbi.type != SB_ITEM_END)){
		if(sbi.type == SB_ITEM_REF)
			*(sbi.ref) = ret;
		else if(sbi.type == SB_ITEM_COLOR){
			colorSet = 1;
			ret.cont->color = sbi.color;
		}
	}
	if(!colorSet) ret.cont->color = sbDefaultFgColor;
	return ret;
}
