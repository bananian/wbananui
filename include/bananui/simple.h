#ifndef _BANANUI_SIMPLE_H_
#define _BANANUI_SIMPLE_H_

#include <bananui/widget.h>
#include <bananui/highlevel.h>

enum sbItemType {
	SB_ITEM_END,
	SB_ITEM_BOX,
	SB_ITEM_CONTENT,
	SB_ITEM_COLOR,
	SB_ITEM_REF,
	SB_ITEM_MGN_L,
	SB_ITEM_MGN_R,
	SB_ITEM_MGN_T,
	SB_ITEM_MGN_B,
	SB_ITEM_ERROR
};

struct sbItem {
	enum sbItemType type;
	union {
		bBoxWidget *box;
		bContentWidget *cont;
		bWidgetColor color;
		struct sbItem *ref;
		const char *err;
		double margin;
	};
};

static const struct sbItem sbEND = {
	.type = SB_ITEM_END
};

/* User may override these, and should if themes are to be used */
extern bWidgetColor sbDefaultBgColor, sbDefaultFgColor;

struct sbItem sbRGBA(double r, double g, double b, double a);
struct sbItem sbMGNL(double mgn);
struct sbItem sbMGNR(double mgn);
struct sbItem sbMGNT(double mgn);
struct sbItem sbMGNB(double mgn);
struct sbItem sbREF(struct sbItem *ref);
struct sbItem sbBOX(bContentAlign align, bBoxDirection dir,
	double width, double height, ...);
struct sbItem sbTEXT(bContentAlign align, const char *txt,
	PangoWeight wt, double size, ...);
struct sbItem sbIMAGE(bContentAlign align, const char *filename, ...);
struct sbItem sbICON(bContentAlign align, const char *name,
	int size, ...);
#endif
