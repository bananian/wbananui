/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_KEYS_H_
#define _BANANUI_KEYS_H_

#include <xkbcommon/xkbcommon.h>

/* This must stay in sync with comp/deviceinfo.c
 * (where the keys are assigned from hardware keycodes)
 */

#define BANANUI_KEY_SoftLeft XKB_KEY_Tab
#define BANANUI_KEY_SoftRight XKB_KEY_Menu
#define BANANUI_KEY_Star XKB_KEY_Control_L
#define BANANUI_KEY_Pound XKB_KEY_Alt_L
#define BANANUI_KEY_LidClosed XKB_KEY_XF86Suspend
#define BANANUI_KEY_LidOpened XKB_KEY_XF86Display
#define BANANUI_KEY_LidFullOpened XKB_KEY_Control_R
#define BANANUI_KEY_Back XKB_KEY_BackSpace
#define BANANUI_KEY_EndCall XKB_KEY_Escape
#define BANANUI_KEY_Call XKB_KEY_F3
#define BANANUI_KEY_Assistant XKB_KEY_F1
#define BANANUI_KEY_Shortcut XKB_KEY_Print

#endif
