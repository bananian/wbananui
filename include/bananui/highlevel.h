/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BANANUI_BUTTON_H_
#define _BANANUI_BUTTON_H_

#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/utils.h>

/*== BUTTONS ==*/
typedef enum beButtonStyle {
	B_BUTTON_STYLE_PUSHBUTTON,
	B_BUTTON_STYLE_MENU,
	B_BUTTON_STYLE_ICON,
	B_BUTTON_STYLE_CHECKBOX,
	B_BUTTON_STYLE_RADIO
} bButtonStyle;
typedef struct bsButtonWidget {
	/* Use these fields to set the button text and icon
	 * (but not the background color!) */
	bBoxWidget *box;
	bContentWidget *icon;
	bContentWidget *cont;
	bContentWidget *subtitle;

	bWidgetColor default_fg_color, default_bg_color;
	bWidgetColor selected_fg_color, selected_bg_color;

	bButtonStyle style;
	int state, focused;

	bBoxWidget *subbox, *iconbox;
} bButtonWidget;

bButtonWidget *bCreateButtonWidget(const char *text, bButtonStyle st,
		bContentAlign align, int width, int height);
void bSetButtonDefaultColor(bButtonWidget *btn,
	bWidgetColor fg, bWidgetColor bg);
void bSetButtonSelectedColor(bButtonWidget *btn,
	bWidgetColor fg, bWidgetColor bg);
void bSetButtonState(bButtonWidget *btn, int state);

/*== INPUT BOXES ==*/
typedef enum beInputStyle {
	B_INPUT_STYLE_SINGLELINE,
	B_INPUT_STYLE_MULTILINE
} bInputStyle;
typedef enum beInputMode {
	B_INPUT_MODE_ALPHA,
	B_INPUT_MODE_NUMBER,
	B_INPUT_MODE_PHONE,
	B_INPUT_MODE_PASSWORD
} bInputMode;
typedef struct bsInputWidget {
	bBoxWidget *box;
	wchar_t *text;
	bEventHandler *change;

	bInputMode mode;
	bInputStyle st;
	int focused, active, cursor;
	bWindow *wnd;

	bWidgetColor default_bg_color, selected_bg_color;
	bWidgetColor text_fg_color, text_bg_color;
	size_t bufsize;

	bBoxWidget *outerbox, *innerbox, *textbox;
	bContentWidget *cont;
} bInputWidget;

bInputWidget *bCreateInputWidget(bWindow *wnd, bInputStyle st, bInputMode mode);

char *bGetInputText(bInputWidget *inp);
int bSetInputText(bInputWidget *inp, const char *text);

#endif
