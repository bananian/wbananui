#ifndef _BANANUI_WINDOW_TITLE_H_
#define _BANANUI_WINDOW_TITLE_H_

/* Renders as a "bell" emoji. */
#define TITLE_PREFIX_ALERT_SCREEN "\xf0\x9f\x94\x94"
#define TITLE_PREFIX_ALERT_SCREEN_LEN 4
/* Renders as a two zero-width spaces. Window must be fullscreen! */
#define TITLE_PREFIX_TOPBAR_OVERLAP "\xe2\x80\x8b\xe2\x80\x8b"
#define TITLE_PREFIX_TOPBAR_OVERLAP_LEN 6

#endif
