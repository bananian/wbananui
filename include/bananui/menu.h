#ifndef _BANANUI_MENU_H_
#define _BANANUI_MENU_H_

#include <bananui/view.h>
#include <bananui/highlevel.h>

typedef struct bsMenuView {
	bView *view;
	struct bsMenuView *prev;
} bMenuView;

typedef struct bsMenu {
	bWindow *wnd;
	bMenuView *current;
	void *userdata;
} bMenu;

bMenu *bCreateMenuView(bWindow *wnd);
int bPopMenu(bMenu *m);
void bSetMenuTitle(bMenu *m, const char *title);

typedef void (*bMenuInitFunc)(bMenu *m);
typedef int (*bMenuClickFunc)(bMenu *m, void *arg);
typedef int (*bMenuButtonFunc)(bMenu *m, bButtonWidget *btn, void *arg);

typedef struct bsMenuEntry {
	const char *name, *icon;
	bButtonStyle style;
	bMenuClickFunc click_handler;
	bMenuButtonFunc button_handler;
	void *arg;
	short height, iconsize;
} bMenuEntry;

typedef struct bsMenuDef {
	bMenuEntry *entries;
	size_t num_entries;
	bMenuInitFunc init;
} bMenuDef;

int bOpenMenuPrivate(bMenu *m, bMenuDef *def);

static inline int bOpenMenuHandlerWrapper(bMenu *m, void *arg) {
	bOpenMenuPrivate(m, arg);
	/*
	 * This is an event handler. bOpenMenuPrivate manipulates events,
	 * so event processing has to be stopped here.
	 */
	return 0;
}

#define B_MENU_ITEMS(_shortname) \
	static bMenuEntry _shortname ## __bMenu_entries__[]

#define B_SUBMENU(_shortname, _name) \
		{ \
			.name = _name, \
			.icon = "go-next-symbolic", \
			.style = B_BUTTON_STYLE_MENU, \
			.click_handler = bOpenMenuHandlerWrapper, \
			.arg = &_shortname ## __bMenu__, \
		}

#define B_DEFINE_MENU(_shortname) \
	static void _shortname ## __bMenu_init__(bMenu *m); \
	static bMenuDef _shortname ## __bMenu__ = { \
		.entries = _shortname ## __bMenu_entries__, \
		.num_entries = sizeof(_shortname ## __bMenu_entries__) / \
			sizeof(bMenuEntry), \
		.init = _shortname ## __bMenu_init__, \
	}; \
	static void _shortname ## __bMenu_init__(bMenu *m) \

#define B_OPEN_MENU(_m, _shortname) bOpenMenuPrivate((_m), &_shortname ## __bMenu__)

#endif
