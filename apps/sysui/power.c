#include <stdlib.h>
#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/softkey.h>
#include <bananui/view.h>
#include <bananui/highlevel.h>
#include "power.h"

static int do_cmd(void *param, void *data)
{
	system((const char*)data);
	return 1;
}

bView *createPowerView(void)
{
	bButtonWidget *poweroff, *reboot, *reset;
	bView *view = bCreateView(0);

	if(!view) return NULL;

	view->bg->color = bColorFromTheme("background");

	poweroff = bCreateButtonWidget("Power off", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, -1);
	reboot = bCreateButtonWidget("Reboot", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, -1);
	reset = bCreateButtonWidget("Reset UI", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, -1);

	if(!poweroff || !reboot || !reset){
		bDestroyView(view);
		return NULL;
	}

	bAddBoxToBox(view->body, poweroff->box);
	bAddBoxToBox(view->body, reboot->box);
	bAddBoxToBox(view->body, reset->box);

	bRegisterEventHandler(&poweroff->box->click, do_cmd, "/sbin/poweroff");
	bRegisterEventHandler(&reboot->box->click, do_cmd, "/sbin/reboot");
	bRegisterEventHandler(&reset->box->click, do_cmd, "pkill -fx bananui-compositor");

	return view;
}
