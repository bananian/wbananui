#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/view.h>
#include "power.h"

static int handle_keydown(void *param, void *data)
{
	xkb_keysym_t *sym = param;

	if(*sym == XKB_KEY_BackSpace || *sym == XKB_KEY_Escape)
		exit(0);

	return 1;
}

int main(int argc, char * const *argv)
{
	bView *view;
	bWindow *wnd;

	bGlobalAppClass = "sysui";

	if(argc < 2){
		fprintf(stderr,
			"Invalid invocation: Please set argv[0] and argv[1]\n");
		return 1;
	}

	if(0 == strcmp(argv[0], "power")){
		view = createPowerView();
		if(!view) return 1;
	}
	else {
		fprintf(stderr,
			"Must be called by bananui with special arguments\n");
		return 1;
	}

	wnd = bCreateWindow(argv[1]);
	if(!wnd) return 1;

	bRegisterEventHandler(&wnd->keydown, handle_keydown, NULL);

	bShowView(wnd, view);
	bRedrawWindow(wnd);

	while(!wnd->closing){
		int res, fd;
		fd_set fds;

		fd = bGetWindowFd(wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		res = select(fd+1, &fds, NULL, NULL, NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			return 1;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(wnd);
			}
		}
	}

	return 0;
}
