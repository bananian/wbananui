#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <bananui/view.h>
#include <bananui/window.h>
#include <bananui/simple.h>
#include "iris.h"
#define START_TEXT "Press START to turn on."
#define NOINFO_TEXT "No detailed information available."
#include "radioUI.h"

struct application_state {
	int radio;
	unsigned int rangelow, rangehigh, freq;
	unsigned int on : 1;
	unsigned int running : 1;
	bWindow *wnd;
	bView *view;
	bContentWidget *text_pri, *text_sec, *text_freq;
};

static int render_widgets(struct application_state *as)
{
	struct sbItem widgets =
		radioUI(&as->text_pri, &as->text_sec, &as->text_freq);
	if(widgets.type == SB_ITEM_ERROR){
		fprintf(stderr, "Rendering simple widgets failed: %s\n",
			widgets.err);
		return 1;
	}
	bAddBoxToBox(as->view->body, widgets.box);
	return 0;
}

static int eventloop(int pfd, int fd)
{
	unsigned char buf[256]; /* 256 is the expected size */
	size_t len;
	while(0 == iris_evq(fd, buf, &len)){
		int res = write(pfd, buf, len);
		if(res < 0)
			perror("write to pipe");
		else if(res != len)
			fprintf(stderr, "incomplete write to pipe\n");
	}
	return 1;
}

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	xkb_keysym_t *sym = param;

	if(*sym == XKB_KEY_Escape){
		as->running = 0;
		return 1;
	}

	if(as->radio < 0){
		if(*sym == XKB_KEY_Return) as->running = 0;
		return 1;
	}

	if(!as->on && *sym == XKB_KEY_Return){
		iris_init(as->radio, &as->rangelow, &as->rangehigh);
	}
	if(!as->on)
		return 1;

	if(*sym == XKB_KEY_Return){
		iris_disable(as->radio);
	}
	else if(*sym == XKB_KEY_Right){
		as->freq += 0.05 * 16000.0;
		if(as->freq > as->rangehigh) as->freq = as->rangelow;
		iris_freq(as->radio, as->freq);
	}
	else if(*sym == XKB_KEY_Left){
		as->freq -= 0.05 * 16000.0;
		if(as->freq < as->rangelow) as->freq = as->rangehigh;
		iris_freq(as->radio, as->freq);
	}

	return 1;
}

static void disp_cb(void *data, enum disp_ev ev, const char *text)
{
	struct application_state *as = data;
	switch(ev){
		case DISP_RADIO_OFF:
			as->on = 0;
			bSetTextContent(as->text_pri, START_TEXT,
				PANGO_WEIGHT_BOLD, 18);
			bSetTextContent(as->text_sec, "",
				PANGO_WEIGHT_NORMAL, 16);
			bSetSoftkeyText(as->view->sk, "", "START", "");
			break;
		case DISP_RADIO_ON:
			as->on = 1;
			bSetTextContent(as->text_sec, "...",
				PANGO_WEIGHT_NORMAL, 16);
			bSetSoftkeyText(as->view->sk, "", "STOP", "");
			if(as->freq == 0) as->freq = as->rangelow;
			iris_freq(as->radio, as->freq);
			break;
		case DISP_RDS_OFF:
			bSetTextContent(as->text_pri, "",
				PANGO_WEIGHT_NORMAL, 16);
			bSetTextContent(as->text_sec, NOINFO_TEXT,
				PANGO_WEIGHT_NORMAL, 16);
			break;
		case DISP_TEXT_PRI:
			bSetTextContent(as->text_pri, text,
				PANGO_WEIGHT_BOLD, 18);
			break;
		case DISP_TEXT_SEC:
			bSetTextContent(as->text_sec, text,
				PANGO_WEIGHT_BOLD, 16);
			break;
		case DISP_FREQ_UPDATE: {
			char freqtext[7];
			as->freq = iris_get_freq(as->radio);
			snprintf(freqtext, 7, "%.2f\n", as->freq / 16000.0);
			bSetTextContent(as->text_freq, freqtext,
				PANGO_WEIGHT_BOLD, 30);
			break;
		}
		default:
		       return;
	}
	bRedrawWindow(as->wnd);
}

int main(void)
{
	pid_t child;
	int pipefd[2], ret = 0;
	struct application_state as;

	as.freq = 0;
	as.radio = open("/dev/radio0", O_RDWR);
	if(as.radio < 0){
		perror("open /dev/radio0");
	}

	if(pipe(pipefd)){
		perror("pipe");
		return 1;
	}

	switch((child = fork())){
		case -1:
			perror("fork");
			return 1;
		case 0:
			close(pipefd[0]);
			exit(eventloop(pipefd[1], as.radio));
			break;
		default:
			close(pipefd[1]);
			break;
	}

	sbDefaultFgColor = bColorFromTheme("text");
	sbDefaultBgColor = bRGBA(0.0, 0.0, 0.0, 0.0);

	as.view = bCreateView(1);
	as.view->sk = bCreateSoftkeyPanel();
	bSetSoftkeyText(as.view->sk, "", as.radio < 0 ? "EXIT" : "START", "");
	as.view->bg->color = bColorFromTheme("background");

	if(render_widgets(&as)){
		ret = 1;
		goto end;
	}

	if(as.radio < 0){
		bSetTextContent(as.text_pri, "Radio unavailable",
			PANGO_WEIGHT_BOLD, 18);
	}

	as.wnd = bCreateWindow("FM Radio");
	if(!as.wnd){
		ret = 1;
		goto end;
	}
	bRegisterEventHandler(&as.wnd->keydown, handle_keydown, &as);
	bShowView(as.wnd, as.view);
	as.running = 1;
	bRedrawWindow(as.wnd);

	while(as.running && !as.wnd->closing){
		int res, fd;
		fd_set fds;

		fd = bGetWindowFd(as.wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		FD_SET(pipefd[0], &fds);

		res = select(fd+1, &fds, NULL, NULL, NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			ret = 1;
			break;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(as.wnd);
			}
			if(FD_ISSET(pipefd[0], &fds)){
				unsigned char buf[256];
				int res = read(pipefd[0], buf, 256);
				if(res < 0){
					perror("read from pipe");
					ret = 1;
					break;
				}
				else if(res == 0){
					fprintf(stderr, "Child closed pipe!\n");
					ret = 1;
					break;
				}
				iris_evq_process(as.radio, buf, res,
					disp_cb, &as);
			}
		}
	}
end:
	kill(child, SIGTERM);
	return ret;
}
