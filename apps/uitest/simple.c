/* vim: set sw=2 sts=2 et: */
#include <bananui/simple.h>
#include <assert.h>
#include "simple.h"

static const char *text =
"This text was rendered using the 'simple' interface.";

void render_simple_widgets(bBoxWidget *dest)
{
  sbDefaultBgColor = bColorFromTheme("background");
  sbDefaultFgColor = bColorFromTheme("text");
  struct sbItem black = sbRGBA(0.0, 0.0, 0.0, 1.0);
  struct sbItem brown = sbRGBA(0.2, 0.1, 0.0, 1.0);
  struct sbItem frg;
  struct sbItem widgets = sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1,
    sbBOX(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0,
      sbTEXT(B_ALIGN_START, "Left", PANGO_WEIGHT_NORMAL, 16,
        black, sbEND),
      sbTEXT(B_ALIGN_END, "Right", PANGO_WEIGHT_NORMAL, 16,
        black, sbEND),
      sbRGBA(1.0, 1.0, 0.9, 1.0),
      sbEND
    ),
    sbBOX(B_ALIGN_CENTER, B_BOX_HBOX, -1, -1,
      sbTEXT(B_ALIGN_CENTER, text, PANGO_WEIGHT_NORMAL, 20, sbEND),
      sbEND
    ),
    sbBOX(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0,
      sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1, sbEND),
      sbBOX(B_ALIGN_CENTER, B_BOX_VBOX, -1, -1,
        sbTEXT(B_ALIGN_CENTER, "Frog", PANGO_WEIGHT_NORMAL, 20, sbREF(&frg),
          brown, sbEND),
        sbRGBA(0.0, 1.0, 0.0, 1.0),
        sbMGNB(20.0),
        sbEND
      ),
      sbBOX(B_ALIGN_START, B_BOX_VBOX, -1, -1, sbEND),
      sbEND
    ),
    sbEND
  );
  if(widgets.type == SB_ITEM_ERROR)
    fprintf(stderr, "Rendering simple widgets failed: %s\n",
      widgets.err);
  else {
    assert(widgets.type == SB_ITEM_BOX);
    bAddBoxToBox(dest, widgets.box);
  }
  assert(frg.type == SB_ITEM_CONTENT);
  bSetTextContent(frg.cont, "FrogFrog", PANGO_WEIGHT_NORMAL, 20);
}
