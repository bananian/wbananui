#include <sys/select.h>
#include <cairo.h>
#include <pango/pangocairo.h>
#include <unistd.h>
#include <assert.h>
#include <xkbcommon/xkbcommon.h>
#include <bananui/view.h>
#include <bananui/widget.h>
#include <bananui/softkey.h>
#include <bananui/window.h>
#include <bananui/highlevel.h>
#include "simple.h"

static const char longtext[] =
"This is a long text\n"
"With newlines\n\n"
"With paragraphs\n\n"
"With emojis 🔎 and long long long long long long long long long long long long long long long long long long long long long long long lines\n"
"And very looooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong words";

#define NUM_VIEWS 5

struct application_state {
	bView *views[NUM_VIEWS];
	bButtonWidget *btn, *pbtn;
	bContentWidget *text;
	bWindow *wnd;
	int redraw, clicked;
	int scroll, xscroll;
	int view;
};

static int handle_click_0(void *param, void *data)
{
	fprintf(stderr, "[1] Clicked\n");
	return 1;
}

static int handle_click_1(void *param, void *data)
{
	struct application_state *as = data;
	fprintf(stderr, "[2] Clicked\n");
	as->clicked = !as->clicked;
	bSetTextContent(as->text, as->clicked ? "Clicked!" : "",
		as->clicked ? PANGO_WEIGHT_HEAVY : PANGO_WEIGHT_NORMAL,
		as->clicked ? 17 : 14);
	bSetButtonState(as->btn, as->clicked);
	as->redraw = 1;
	return 0;
}
static int handle_click_2(void *param, void *data)
{
	assert(0 && "This handler should be skipped!");
	return 0;
}
static int handle_change(void *param, void *data)
{
	fprintf(stderr, "Input text changed: %s\n", (const char *)param);
	return 1;
}

static int handle_keydown(void *param, void *data)
{
	struct application_state *as = data;
	xkb_keysym_t *sym = param;
	if(*sym >= 0x20 && *sym < 0x7f){
		fprintf(stderr, "Key down: %c\n", (char)(*sym));
	}
	else {
		fprintf(stderr, "Key down: 0x%x\n", *sym);
	}
	switch(*sym){
		case XKB_KEY_Left:
			bHideView(as->wnd, as->views[as->view]);
			as->view--;
			if(as->view < 0) as->view = NUM_VIEWS-1;
			bShowView(as->wnd, as->views[as->view]);
			as->redraw = 1;
			break;
		case XKB_KEY_Right:
			bHideView(as->wnd, as->views[as->view]);
			as->view++;
			if(as->view >= NUM_VIEWS) as->view = 0;
			bShowView(as->wnd, as->views[as->view]);
			as->redraw = 1;
			break;
		case XKB_KEY_Escape:
		case XKB_KEY_BackSpace:
			exit(0);
			break;
	}
	return 1;
}

int main(int argc, char * const *argv)
{
	bWindow *wnd;
	bBoxWidget *min, *max, *fixed;
	bContentWidget *bodytext, *bodytext2, *bodytext3, *bodytext4;
	bContentWidget *mintext, *maxtext, *fixedtext;
	bButtonWidget *v0btn1;
	bButtonWidget *v1btn1, *v1btn2, *v1btn3, *v1btn4, *v1btn5, *v1btn6;
	bButtonWidget *v3btn1, *v3btn2;
	bButtonWidget *pbtn, *pbtn2;
	bInputWidget *inp;
	struct application_state as;
	int animation = 0, running = 1;
	double alpha = 1;

	wnd = bCreateSizedWindow(TITLE_PREFIX_ALERT_SCREEN, 0, 200);
	if(!wnd) return 1;

	as.view = 0;

	as.views[0] = bCreateView(1);
	as.views[1] = bCreateView(1);
	as.views[2] = bCreateView(1);
	as.views[3] = bCreateView(0);
	as.views[4] = bCreateView(0);

	min = bCreateBoxWidget(B_ALIGN_END, B_BOX_VBOX, 0, 0);
	max = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX, -1, -1);
	fixed = bCreateBoxWidget(B_ALIGN_END, B_BOX_VBOX, 20, 20);

	v0btn1 = bCreateButtonWidget("Test Button", B_BUTTON_STYLE_CHECKBOX,
		B_ALIGN_START, -1, 60);
	v1btn1 = bCreateButtonWidget("Test test", B_BUTTON_STYLE_RADIO,
		B_ALIGN_START, -1, 60);
	v1btn2 = bCreateButtonWidget("Test test test", B_BUTTON_STYLE_RADIO,
		B_ALIGN_START, -1, 60);
	v1btn3 = bCreateButtonWidget("Test test test test",
		B_BUTTON_STYLE_RADIO, B_ALIGN_START, -1, 60);
	v1btn4 = bCreateButtonWidget("Test test test test test",
		B_BUTTON_STYLE_RADIO, B_ALIGN_START, -1, 60);
	v1btn5 = bCreateButtonWidget(
		"Test test test test test test test test test test test",
		B_BUTTON_STYLE_RADIO, B_ALIGN_START, -1, 60);
	v1btn6 = bCreateButtonWidget("A button with an icon",
		B_BUTTON_STYLE_ICON, B_ALIGN_START, -1, 60);
	v3btn1 = bCreateButtonWidget("First button", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, -1);
	v3btn2 = bCreateButtonWidget("Second button", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, -1);
	pbtn = bCreateButtonWidget("Push me", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, 40);
	pbtn2 = bCreateButtonWidget("Push me too", B_BUTTON_STYLE_PUSHBUTTON,
		B_ALIGN_START, -1, 40);
	inp = bCreateInputWidget(wnd,
		B_INPUT_STYLE_SINGLELINE,
		B_INPUT_MODE_ALPHA);
	bSetInputText(inp, "Enter something");

	bodytext = bCreateContentWidget(B_ALIGN_START, 0);
	bodytext2 = bCreateContentWidget(B_ALIGN_START, 0);
	bodytext3 = bCreateContentWidget(B_ALIGN_START, 0);
	bodytext4 = bCreateContentWidget(B_ALIGN_START, 0);

	mintext = bCreateContentWidget(B_ALIGN_START, 0);
	maxtext = bCreateContentWidget(B_ALIGN_START, 0);
	fixedtext = bCreateContentWidget(B_ALIGN_START, 0);

	as.views[0]->sk = bCreateSoftkeyPanel();
	as.views[2]->sk = bCreateSoftkeyPanel();

	as.views[1]->header = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX,
		-1, 25);

	render_simple_widgets(as.views[4]->body);
	bAddBoxToBox(as.views[0]->body, min);
	bAddContentToBox(min, mintext);
	bAddBoxToBox(as.views[0]->body, max);
	bAddContentToBox(max, maxtext);
	bAddBoxToBox(as.views[0]->body, fixed);
	bAddBoxToBox(as.views[0]->body, v0btn1->box);
	bAddBoxToBox(as.views[0]->body, pbtn->box);
	bAddBoxToBox(as.views[0]->body, inp->box);
	bAddContentToBox(fixed, fixedtext);
	bAddContentToBox(as.views[0]->body, bodytext);
	bAddBoxToBox(as.views[0]->body, pbtn2->box);
	bAddContentToBox(as.views[0]->body, bodytext2);

	bAddBoxToBox(as.views[1]->body, v1btn1->box);
	bAddContentToBox(as.views[1]->body, bodytext3);
	bAddBoxToBox(as.views[1]->body, v1btn2->box);
	bAddBoxToBox(as.views[1]->body, v1btn3->box);
	bAddBoxToBox(as.views[1]->body, v1btn4->box);
	bAddBoxToBox(as.views[1]->body, v1btn5->box);
	bAddBoxToBox(as.views[1]->body, v1btn6->box);

	bAddContentToBox(as.views[2]->body, bodytext4);

	bAddBoxToBox(as.views[3]->body, v3btn1->box);
	bAddBoxToBox(as.views[3]->body, v3btn2->box);

	bSetTextContent(bodytext, longtext, PANGO_WEIGHT_NORMAL, 17);
	bSetTextContent(bodytext2, "small text", PANGO_WEIGHT_NORMAL, 17);
	bSetTextContent(bodytext3, "Test text", PANGO_WEIGHT_BOLD, 20);
	bSetTextContent(bodytext4, longtext, PANGO_WEIGHT_BOLD, 20);
	bSetTextContent(maxtext, "", PANGO_WEIGHT_NORMAL, 14);
	bSetTextContent(mintext, "", PANGO_WEIGHT_NORMAL, 14);
	bSetSoftkeyText(as.views[0]->sk, "Hello", "WONDERFUL", "World");
	bSetSoftkeyText(as.views[2]->sk, "", "ANOTHER", "Test");

	as.views[0]->bg->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	as.views[1]->bg->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	as.views[1]->header->color = bRGBA(0.0, 1.0, 0.0, 1.0);
	as.views[2]->bg->color = bRGBA(1.0, 0.0, 0.0, 1.0);
	as.views[3]->bg->color = bRGBA(0.0, 1.0, 0.0, 1.0);

	min->color = bRGBA(1.0, 0.0, 0.0, 1.0);
	max->color = bRGBA(0.0, 1.0, 0.0, 1.0);
	fixed->color = bRGBA(0.0, 0.0, 1.0, 1.0);

	bodytext4->color = bRGBA(1.0, 1.0, 1.0, 1.0);

	bSetTextContent(v0btn1->subtitle, "Hello Subtitle",
		PANGO_WEIGHT_NORMAL, 14);
	bSetTextContent(v3btn1->subtitle, "With a subtitle",
		PANGO_WEIGHT_NORMAL, 14);
	bLoadImageFromIcon(v1btn6->icon, "document-send-symbolic", 32);

	mintext->color = bRGBA(1.0, 1.0, 1.0, 1.0);

	as.scroll = 0;
	as.xscroll = 0;
	as.text = mintext;
	as.btn = v0btn1;
	as.pbtn = pbtn;
	as.redraw = 1;
	as.clicked = 0;
	as.wnd = wnd;

	bRegisterEventHandler(&v0btn1->box->click, handle_click_2, &as);
	bRegisterEventHandler(&v0btn1->box->click, handle_click_1, &as);
	bRegisterEventHandler(&v0btn1->box->click, handle_click_0, &as);
	bRegisterEventHandler(&inp->change, handle_change, &as);

	bRegisterEventHandler(&wnd->keydown, handle_keydown, &as);
	
	bShowView(wnd, as.views[0]);

	while(running && !wnd->closing){
		int res, fd;
		struct timeval to = {0, 0};
		fd_set fds;

		fd = bGetWindowFd(wnd);

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		res = select(fd+1, &fds, NULL, NULL, as.redraw ? &to : NULL);

		if(res < 0 && errno != EINTR){
			perror("select");
			return 1;
		}
		if(res > 0){
			if(FD_ISSET(fd, &fds)){
				bHandleWindowEvent(wnd);
			}
		}

		if(as.redraw){
			bRedrawWindow(wnd);
			as.redraw = 0;
		}
	}

	return 0;
}
