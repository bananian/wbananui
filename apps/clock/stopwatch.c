#include <stdlib.h>
#include <bananui/window.h>
#include <bananui/widget.h>
#include <bananui/softkey.h>
#include <bananui/view.h>
#include "stopwatch.h"
#include "clock.h"

struct stopwatch {
	bWindow *wnd;
	bView *view;
	bContentWidget *txt, *headertxt;
	bBoxWidget *txtbox;
};

static int fill_stopwatch_header(struct stopwatch *sw)
{
	sw->view->header->color = bColorFromTheme("header:background");

	sw->headertxt = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!sw->headertxt) return 0;
	bAddContentToBox(sw->view->header, sw->headertxt);

	sw->headertxt->color = bColorFromTheme("header:text");
	bSetTextContent(sw->headertxt, "Stopwatch", PANGO_WEIGHT_NORMAL, 16);

	return 1;
}

static int fill_stopwatch_body(struct stopwatch *sw)
{
	sw->txtbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
			-1, -1);
	if(!sw->txtbox) return 0;
	bAddBoxToBox(sw->view->body, sw->txtbox);

	sw->txt = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!sw->txt) return 0;
	sw->txt->color = bColorFromTheme("secondary");
	bAddContentToBox(sw->txtbox, sw->txt);

	bSetTextContent(sw->txt, "00:00:00", PANGO_WEIGHT_BOLD, 48);

	return 1;
}

static int stopwatch_keydown(void *param, void *data)
{
	struct stopwatch *sw = data;
	xkb_keysym_t key = *((xkb_keysym_t*)param);
	if(key == XKB_KEY_Return){
		bSetTextContent(sw->txt, "00:00:01", PANGO_WEIGHT_BOLD, 48);
		bRedrawWindow(sw->wnd);
	}
	return 1;
}

bView *create_stopwatch_view(bWindow *wnd)
{
	struct stopwatch *sw = calloc(1, sizeof(struct stopwatch));
	if(!sw) return NULL;
	sw->wnd = wnd;
	sw->view = bCreateView(0);
	if(!sw->view) goto free_sw;

	sw->view->sk = bCreateSoftkeyPanel();
	if(!sw->view->sk) goto free_view;

	sw->view->header = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
			-1, 30);
	if(!sw->view->header) goto free_sk;

	sw->view->bg->color = color_back;

	if(!fill_stopwatch_header(sw)) goto free_header;
	if(!fill_stopwatch_body(sw)) goto free_header;

	bSetSoftkeyText(sw->view->sk, "", "START", "");

	bRegisterEventHandler(&sw->view->keydown, stopwatch_keydown, sw);

	return sw->view;

free_header:
	bDestroyBoxRecursive(sw->view->header);
free_sk:
	bDestroySoftkeyPanel(sw->view->sk);
free_view:
	bDestroyView(sw->view);
free_sw:
	free(sw);
	return NULL;
}
