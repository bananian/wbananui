/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <xkbcommon/xkbcommon.h>
#include <bananui/window.h>
#include <assert.h>
#include <time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "xdg-shell-client-protocol.h"
#include "text-input-client-protocol.h"

struct {
	struct wl_display *disp;
} conn = { NULL };

static void keyboard_handle_keymap(void *data, struct wl_keyboard *kb,
		uint32_t format, int32_t fd, uint32_t size)
{
	bWindow *wnd = data;
	char *buf;
	if(format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1){
		fprintf(stderr, "Incompatible keymap format\n");
		return;
	}
	buf = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if(buf == MAP_FAILED){
		perror("Failed to map keymap");
		return;
	}
	xkb_keymap_unref(wnd->keymap);
	wnd->keymap = xkb_keymap_new_from_buffer(wnd->xkbctx,
		buf, size-1, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	if(!wnd->keymap){
		munmap(buf, size);
		fprintf(stderr, "Failed to load keymap\n");
		return;
	}
	munmap(buf, size);
	wnd->xkbstate = xkb_state_new(wnd->keymap);
	if(!wnd->xkbstate){
		xkb_keymap_unref(wnd->keymap);
		fprintf(stderr, "Failed to create keyboard state\n");
		return;
	}
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Loaded keymap\n");
#endif
}
static void keyboard_handle_enter(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface,
		struct wl_array *keys)
{
	bWindow *win = data;
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Keyboard enter\n");
#endif
	bTriggerEvent(win->focusin, NULL);
}
static void keyboard_handle_leave(void *data, struct wl_keyboard *kb,
		uint32_t serial, struct wl_surface *surface)
{
	bWindow *win = data;
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Keyboard leave\n");
#endif
	bTriggerEvent(win->focusout, NULL);
}
static void keyboard_handle_key(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
	bWindow *win = data;
	const xkb_keysym_t *syms;
	xkb_keycode_t kc = key+8;
	size_t numsyms;
	int i;
	numsyms = xkb_state_key_get_syms(win->xkbstate, kc, &syms);
	for(i = 0; i < numsyms; i++){
		xkb_keysym_t sym = syms[i];
		if(state == WL_KEYBOARD_KEY_STATE_PRESSED){
			bTriggerEvent(win->keydown, &sym);
		}
		else if(state == WL_KEYBOARD_KEY_STATE_RELEASED){
			bTriggerEvent(win->keyup, &sym);
		}
	}
}
static void keyboard_handle_modifiers(void *data, struct wl_keyboard *kb,
		uint32_t serial, uint32_t depressed,
		uint32_t latched, uint32_t locked, uint32_t group)
{
	bWindow *win = data;
	xkb_state_update_mask(win->xkbstate, depressed, latched, locked,
		group, group, group);
}
static void keyboard_handle_repeat_info(void *data, struct wl_keyboard *kb,
		int32_t rate, int32_t delay)
{
	fprintf(stderr, "keyboard_handle_repeat_info\n");
}

static const struct wl_keyboard_listener keyboard_listener = {
	keyboard_handle_keymap,
	keyboard_handle_enter,
	keyboard_handle_leave,
	keyboard_handle_key,
	keyboard_handle_modifiers,
	keyboard_handle_repeat_info
};

static void xdg_wm_base_handle_ping(void *data, struct xdg_wm_base *shell,
	uint32_t serial)
{
	xdg_wm_base_pong(shell, serial);
}
static void xdg_surface_handle_configure(void *data,
	struct xdg_surface *surface, uint32_t serial)
{
	bWindow *wnd = data;
	xdg_surface_ack_configure(surface, serial);
	wnd->configured_surface = 1;
}
static void shm_randname(char *buf)
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	long r = ts.tv_nsec;
	for (int i = 0; i < 6; ++i) {
		buf[i] = 'A'+(r&15)+(r&16)*2;
		r >>= 5;
	}
}

static int create_shm_file(void)
{
	int retries = 100;
	do {
		char name[] = "/wl_shm-XXXXXX";
		shm_randname(name + sizeof(name) - 7);
		--retries;
		int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
		if (fd >= 0) {
			shm_unlink(name);
			return fd;
		}
	} while (retries > 0 && errno == EEXIST);
	perror("shm create failed");
	return -1;
}

static int allocate_shm(size_t size)
{
	int fd = create_shm_file();
	if (fd < 0)
		return -1;
	int ret;
	do {
		ret = ftruncate(fd, size);
	} while (ret < 0 && errno == EINTR);
	if (ret < 0) {
		close(fd);
		return -1;
	}
	return fd;
}
static void xdg_toplevel_handle_configure(void *data,
	struct xdg_toplevel *toplevel, int32_t width, int32_t height,
	struct wl_array *states)
{
	bWindow *wnd = data;
	if(wnd->configured_toplevel) return;
	wnd->height = height ? height : 290;
	wnd->width = width ? width : 240;
	wnd->configured_toplevel = 1;
#if BANANUI_VERBOSE
	fprintf(stderr, "[DEBUG] Window size=%dx%d\n", width, height);
#endif
}
static void xdg_toplevel_handle_close(void *data, struct xdg_toplevel *toplevel)
{
	bWindow *wnd = data;
	wnd->closing = 1;
}

static const struct xdg_wm_base_listener wm_base_listener = {
	xdg_wm_base_handle_ping
};
static const struct xdg_surface_listener xdg_surface_listener = {
	xdg_surface_handle_configure
};
static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	xdg_toplevel_handle_configure,
	xdg_toplevel_handle_close
};

static void text_input_handle_enter(void *data, struct zwp_text_input_v3 *ti,
	struct wl_surface *surface)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	bTriggerEvent(wnd->text_input_enter, NULL);
}

static void text_input_handle_leave(void *data, struct zwp_text_input_v3 *ti,
	struct wl_surface *surface)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	bTriggerEvent(wnd->text_input_leave, NULL);
}

static void text_input_handle_preedit_string(void *data,
	struct zwp_text_input_v3 *ti,
	const char *text, int32_t cursor_begin, int32_t cursor_end)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	if(wnd->tis.preedit) free(wnd->tis.preedit);
	wnd->tis.preedit = strdup(text);
}

static void text_input_handle_commit_string(void *data,
	struct zwp_text_input_v3 *ti,
	const char *text)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	if(wnd->tis.commit_string) free(wnd->tis.commit_string);
	wnd->tis.commit_string = strdup(text);
}

static void text_input_handle_delete_surrounding_text(void *data,
	struct zwp_text_input_v3 *ti,
	uint32_t before, uint32_t after)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	wnd->tis.delete_before = before;
	wnd->tis.delete_after = after;
}

static void text_input_handle_done(void *data,
	struct zwp_text_input_v3 *ti, uint32_t serial)
{
	bWindow *wnd = data;
	assert(ti == wnd->text_input);
	bTriggerEvent(wnd->text_input_update, NULL);
	if(wnd->tis.commit_string){
		free(wnd->tis.commit_string);
		wnd->tis.commit_string = NULL;
	}
	if(wnd->tis.preedit){
		free(wnd->tis.preedit);
		wnd->tis.preedit = NULL;
	}
	wnd->tis.delete_before = wnd->tis.delete_after = 0;
}

static const struct zwp_text_input_v3_listener text_input_listener = {
	text_input_handle_enter,
	text_input_handle_leave,
	text_input_handle_preedit_string,
	text_input_handle_commit_string,
	text_input_handle_delete_surrounding_text,
	text_input_handle_done
};

static void registry_handle_global(void *data, struct wl_registry *reg,
	uint32_t name, const char *interface, uint32_t version)
{
	struct registry_interfaces *interfaces = data;
	if(strcmp(interface, "wl_compositor") == 0){
		interfaces->comp = wl_registry_bind(reg, name,
			&wl_compositor_interface, version > 4 ? 4 : version);
	}
	else if(strcmp(interface, "wl_seat") == 0){
		interfaces->seat = wl_registry_bind(reg, name,
			&wl_seat_interface, version);
	}
	else if(strcmp(interface, "xdg_wm_base") == 0){
		interfaces->wm_base = wl_registry_bind(reg, name,
			&xdg_wm_base_interface, 1);
		xdg_wm_base_add_listener(interfaces->wm_base,
			&wm_base_listener, NULL);
	}
	else if(strcmp(interface, "wl_shm") == 0){
		interfaces->shm = wl_registry_bind(reg, name,
			&wl_shm_interface, 1);
	}
	else if(strcmp(interface, "zwp_text_input_manager_v3") == 0){
		interfaces->text_input = wl_registry_bind(reg, name,
			&zwp_text_input_manager_v3_interface, version);
	}
}
static void registry_handle_global_remove(void *data, struct wl_registry *reg,
	uint32_t name)
{
}

static const struct wl_registry_listener listeners = {
	registry_handle_global,
	registry_handle_global_remove
};

static bWindow *createWindow(const char *title, int fullscreen, int w, int h)
{
	int stride;
	struct wl_registry *reg;
	bWindow *wnd;
	wnd = malloc(sizeof(bWindow));
	if(!wnd){
		fprintf(stderr, "Failed to allocate memory for window\n");
		return NULL;
	}
	wnd->closing = 0;
	wnd->surf = NULL;
	wnd->cr = NULL;
	wnd->configured_surface = 0;
	wnd->configured_toplevel = 0;
	wnd->xkbctx = xkb_context_new(0);
	wnd->keymap = NULL;
	wnd->focusin = wnd->focusout = wnd->keydown = wnd->keyup =
		wnd->fullscreenchange = wnd->redraw = NULL;
	wnd->text_input_update = wnd->text_input_leave = wnd->text_input_enter =
		NULL;
	wnd->tis.preedit = wnd->tis.commit_string =
		NULL;
	wnd->tis.delete_before = wnd->tis.delete_after =
		0;
	if(!conn.disp) conn.disp = wl_display_connect(NULL);
	if(!conn.disp){
		fprintf(stderr, "Failed to connect to wayland display\n");
		free(wnd);
		return NULL;
	}
	wnd->disp = conn.disp; /* for users of bWindow */
	reg = wl_display_get_registry(wnd->disp);
	wl_registry_add_listener(reg, &listeners, &wnd->reg);
	wl_display_roundtrip(wnd->disp);
	wl_registry_destroy(reg);
	if(!wnd->reg.comp){
		fprintf(stderr, "No wl_compositor interface found.\n");
		goto destroy;
	}
	if(!wnd->reg.wm_base){
		fprintf(stderr, "No XDG shell (xdg_wm_base) found.\n");
		goto destroy;
	}
	if(!wnd->reg.seat){
		fprintf(stderr, "No seat available.\n");
		goto destroy;
	}
	if(!wnd->reg.shm){
		fprintf(stderr, "No wl_shm found.\n");
	}
	if(!wnd->reg.text_input){
		fprintf(stderr, "No text input method.\n");
	}

	wnd->keyboard = wl_seat_get_keyboard(wnd->reg.seat);
	if(wnd->keyboard){
		wl_keyboard_add_listener(wnd->keyboard,
			&keyboard_listener, wnd);
	}

	wnd->wlsurf = wl_compositor_create_surface(wnd->reg.comp);
	if(!wnd->wlsurf) goto destroy;

	wnd->xdgsurf =
		xdg_wm_base_get_xdg_surface(wnd->reg.wm_base, wnd->wlsurf);
	if(!wnd->xdgsurf) goto destroy;
	xdg_surface_add_listener(wnd->xdgsurf, &xdg_surface_listener, wnd);
	wnd->toplevel = xdg_surface_get_toplevel(wnd->xdgsurf);
	if(!wnd->toplevel) goto destroy;
	xdg_toplevel_add_listener(wnd->toplevel, &xdg_toplevel_listener, wnd);
	xdg_toplevel_set_title(wnd->toplevel, title);
	wl_surface_commit(wnd->wlsurf);
	if(fullscreen) xdg_toplevel_set_fullscreen(wnd->toplevel, NULL);

	while(!(wnd->configured_toplevel && wnd->configured_surface) &&
		wl_display_dispatch(wnd->disp) >= 0);

	if(w > 0) wnd->width = w;
	else wnd->width += w;
	if(h > 0) wnd->height = h;
	else wnd->height += h;

	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, wnd->width);
	wnd->shmfd = allocate_shm(stride * wnd->height);
	if(wnd->shmfd < 0) goto destroy;
	wnd->pixels = mmap(NULL, stride * wnd->height, PROT_READ | PROT_WRITE,
		MAP_SHARED, wnd->shmfd, 0);
	if(wnd->pixels == MAP_FAILED){
		perror("mmap");
		wnd->pixels = NULL;
		goto destroy;
	}
	wnd->pool = wl_shm_create_pool(wnd->reg.shm, wnd->shmfd,
			stride * wnd->height);
	if(!wnd->pool) goto destroy;
	wnd->buffer = wl_shm_pool_create_buffer(wnd->pool, 0,
			wnd->width, wnd->height, stride,
			WL_SHM_FORMAT_XRGB8888);
	if(!wnd->buffer) goto destroy;
	wnd->surf = cairo_image_surface_create_for_data(wnd->pixels,
			CAIRO_FORMAT_ARGB32, wnd->width, wnd->height, stride);
	if(!wnd->surf) goto destroy;
	wnd->cr = cairo_create(wnd->surf);
	if(!wnd->cr) goto destroy;
	wl_surface_attach(wnd->wlsurf, wnd->buffer, 0, 0);
	wl_surface_damage(wnd->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(wnd->wlsurf);

	if(!wnd->reg.text_input){
		wnd->text_input = NULL;
	}
	else {
		wnd->text_input = zwp_text_input_manager_v3_get_text_input(
			wnd->reg.text_input, wnd->reg.seat);
		if(!wnd->text_input){
			fprintf(stderr,
			"Warning: Failed to create text input interface\n");
		}
		zwp_text_input_v3_add_listener(wnd->text_input,
			&text_input_listener, wnd);
	}
	return wnd;
destroy:
	bDestroyWindow(wnd);
	return NULL;
}

bWindow *bCreateWindow(const char *title)
{
	return createWindow(title, 0, 0, 0);
}

bWindow *bCreateFullscreenWindow(const char *title)
{
	return createWindow(title, 1, 0, 0);
}

bWindow *bCreateSizedWindow(const char *title, int width, int height)
{
	return createWindow(title, 0, width, height);
}

void bDestroyWindow(bWindow *wnd)
{
	int stride =
		cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, wnd->width);
	if(wnd->text_input) zwp_text_input_v3_destroy(wnd->text_input);
	if(wnd->keyboard) wl_keyboard_destroy(wnd->keyboard);
	if(wnd->cr) cairo_destroy(wnd->cr);
	if(wnd->surf) cairo_surface_destroy(wnd->surf);
	if(wnd->buffer) wl_buffer_destroy(wnd->buffer);
	if(wnd->pool) wl_shm_pool_destroy(wnd->pool);
	if(wnd->pixels) munmap(wnd->pixels, stride * wnd->height);
	if(wnd->toplevel) xdg_toplevel_destroy(wnd->toplevel);
	if(wnd->xdgsurf) xdg_surface_destroy(wnd->xdgsurf);
	if(wnd->wlsurf) wl_surface_destroy(wnd->wlsurf);
	if(wnd->shmfd >= 0) close(wnd->shmfd);
	if(wnd->reg.text_input)
		zwp_text_input_manager_v3_destroy(wnd->reg.text_input);
	if(wnd->reg.seat) wl_seat_destroy(wnd->reg.seat);
	if(wnd->reg.shm) wl_shm_destroy(wnd->reg.shm);
	if(wnd->reg.wm_base) xdg_wm_base_destroy(wnd->reg.wm_base);
	free(wnd);
	wl_display_flush(conn.disp);
}

void bRedrawWindow(bWindow *wnd)
{
	cairo_push_group(wnd->cr);
	bTriggerEvent(wnd->redraw, wnd);
	cairo_pop_group_to_source(wnd->cr);
	cairo_paint(wnd->cr);
	wl_surface_attach(wnd->wlsurf, wnd->buffer, 0, 0);
	wl_surface_damage(wnd->wlsurf, 0, 0, wnd->width, wnd->height);
	wl_surface_commit(wnd->wlsurf);
	wl_display_flush(wnd->disp);
}

int bGetWindowFd(bWindow *wnd)
{
	return wl_display_get_fd(wnd->disp);
}

void bHandleWindowEvent(bWindow *wnd)
{
	wl_display_dispatch(wnd->disp);
}
