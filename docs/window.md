# Window API

Defined in: [`<bananui/window.h>`](../include/bananui/window.h)

## Functions

### bCreateWindow

```c
bWindow *bCreateWindow(const char *title)
```

Create a new window and initialize a connection to the compositor if needed.

Arguments:
 - const char \* `title`: The title of the window. See available
   [prefixes](#title-prefixes)

Return value: a new [bWindow](#bwindow)\* or NULL on error.

### bCreateFullscreenWindow

```c
bWindow *bCreateFullscreenWindow(const char *title)
```

Like [bCreateWindow](#bcreatewindow), but hides the title bar.

### bCreateSizedWindow

```c
bWindow *bCreateSizedWindow(const char *title, int width, int height)
```

Arguments:
 - const char \* `title`: The title of the window. See available
   [prefixes](#prefixes)
 - int `width`: The width of the window in pixels if positive or a negative
   or zero value to add to the system-provided default width.
 - int `height`: The height of the window in pixels if positive or a negative
   or zero value to add to the system-provided default height.

Return value: a new [bWindow](#bwindow)\* or NULL on error.

### bGetWindowFd

```c
int bGetWindowFd(bWindow *wnd)
```

Arguments:
 - [bWindow](#bwindow) `wnd`: The window.

Return value: a pollable file descriptor.

Note: Windows may share the same file descriptor!

### bRedrawWindow

```c
void bRedrawWindow(bWindow *wnd)
```

Trigger a redraw, submit the content of the Cairo surface and
send all pending operations to the compositor.
This function performs I/O and might block if the window file descriptor is
not writable at the moment.

Arguments:
 - [bWindow](#bwindow)\* `wnd`: The window to redraw.

### bDestroyWindow

```c
void bDestroyWindow(bWindow *wnd)
```

Close a window and destroy its resources.
This function performs I/O and might block if the window file descriptor is
not writable at the moment.

Arguments:
 - [bWindow](#bwindow)\* `wnd`: The window to destroy.

### bHandleWindowEvent

```c
void bHandleWindowEvent(bWindow *wnd)
```

Receive a message from the compositor. Should be called by an event loop when
the window file descriptor becomes readable.

Arguments:
 - [bWindow](#bwindow): The window.
   Note: since windows can share file descriptors, events may be dispatched
   to other windows too.

## Structs

### bWindow

Constructors: [bCreateWindow](#bcreatewindow),
[bCreateFullscreenWindow](#bcreatefullscreenwindow) and
[bCreateSizedWindow](#bcreatesizedwindow).

This struct contains all data associated with a window. In most use cases,
you will only need the `closing` boolean, which indicates whether the user
has requested the window to close, and the events:

 - `focusin`: Called when the window is activated and gets keyboard focus.
 - `focusout`: Called when the window is deactivated and loses keyboard focus.
 - `keydown`: Called with a pointer to an `xkb_keysym_t` value when a key is
   pressed.
 - `keyup`: Called with a pointer to an `xkb_keysym_t` value when a key is
   released.
 - `redraw`: Called by [bRedrawWindow](#bredrawwindow) when the Cairo context
   is ready for rendering. This is usually handled by the [view](./view.md) API.

```c
struct registry_interfaces {
	struct zwp_text_input_manager_v3 *text_input;
	struct wl_compositor *comp;
	struct wl_seat *seat;
	struct wl_shm *shm;
	struct xdg_wm_base *wm_base;
};

struct text_input_state {
	char *preedit, *commit_string;
	uint32_t delete_before, delete_after;
};

typedef struct {
	/* public */
	bEventHandler *keydown, *keyup, *fullscreenchange,
		*focusin, *focusout,
		*redraw;
	/* public, readonly */
	cairo_surface_t *surf;
	cairo_t *cr;
	int width, height;
        int /* boolean */ closing;
	/* private data that might be useful */
	struct zwp_text_input_v3 *text_input;
	struct wl_surface *wlsurf;
	struct wl_display *disp;
	struct wl_buffer *buffer;
	struct wl_shm_pool *pool;
	unsigned char *pixels;
	int shmfd;
	struct xdg_toplevel *toplevel;
	struct xdg_surface *xdgsurf;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_context *xkbctx;
	struct xkb_state *xkbstate;
	struct text_input_state tis;
	struct registry_interfaces reg;
	int configured_surface, configured_toplevel;
	bEventHandler *text_input_update, *text_input_leave, *text_input_enter;
} bWindow;
```

## Title prefixes

These constants are defined as macros. They can be prepended to a constant
string like this:

```c
bCreateWindow(TITLE_PREFIX_ALERT_SCREEN "title")
```

Title prefixes cannot be combined.

### `TITLE_PREFIX_TOPBAR_OVERLAP`

Only valid for fullscreen windows. Makes the compositor display a transparent
status bar (30px high) over the content.

### `TITLE_PREFIX_ALERT_SCREEN`

Tells the compositor to create a pop-up window that can show up above other
windows. This also makes it display the window title over the content, so
apps should show an empty dark-colored rectangle (at least 30px high)
on the very top of their popup windows.
