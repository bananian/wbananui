# View API

Defined in: [`<bananui/view.h>`](../include/bananui/view.h)

## Functions

### bCreateView

```c
bView *bCreateView(unsigned int scrollable)
```

Arguments:
 - unsigned int `scrollable`: Boolean indicating whether the view is scrollable.
   Only non-scrollable views may use height-stretched boxes in the body.

Return value: a new [bView](#bview)\* or NULL on error.

### bShowView

```c
void bShowView(bWindow *wnd, bView *view)
```

Bind a view to a window. The view will be rendered on the window when
[bRedrawWindow](./window.md#bredrawwindow) is called.

Arguments:
 - [bWindow](./window.md#bwindow)\* `wnd`: The window. It should not have any
   views attached.
 - [bView](#bview)\* `view`: The view. Bindings of it to other windows will not
   be affected.

### bHideView

```c
void bHideView(bWindow *wnd, bView *view)
```

Unbind a view from a window.

Arguments:
 - [bWindow](./window.md#bwindow)\* `wnd`: The window.
 - [bView](#bview)\* `view`: The view. Bindings of it to other windows will not
   be affected.

### bDestroyView

```c
void bDestroyView(bView *view)
```

Destroy the view and its `body` and `bg` widgets. The user must free the other
properties, including the event listeners.

Arguments:
 - [bView](#bview)\* `view`: The view. If NULL, nothing is done.

### bSetFocus

```c
void bSetFocus(bView *view, bBoxWidget *box, int align_at_bottom)
```

Focus a widget in the view. The caller must ensure that no changes
have been made to the view since the window was last redrawn (the
easiest way to ensure this is to redraw the window before calling
this function). The caller must also take care of redrawing the
window after calling this function.

Arguments:
 - [bView](#bview)\* `view`: The view.
 - [bBoxWidget](./widget.md#bboxwidget)\* `box`: The widget to focus.
 - int `align_at_bottom`: Boolean indicating how to scroll the view to
   make the widget visible. If false, the widget is aligned at the top.
   If true, the widget is aligned to the bottom.

## Structs

### bView

Constructor: [bCreateView](#bcreateview).

Fields:
 - **public**: [bBoxWidget](./widget.md#bboxwidget)\* `body`:
   A predefined box widget which content should be added to.
 - **public**: [bBoxWidget](./widget.md#bboxwidget)\* `bg`:
   A predefined box widget which is displayed behind the body. It is
   always expanded to the screen dimensions. Its position is not affected
   by scrolling
 - **public**: [bBoxWidget](./widget.md#bboxwidget)\* `header`:
   Apps may set this to a box widget which will always be displayed
   above a scrollable body.
 - **public**: [bSoftkeyPanel](./softkey.md#bsoftkeypanel)\* `sk`:
   Apps may set this to a softkey panel which will always be displayed
   below a scrollable body.
 - **readonly**: unsigned int `scrollable`:
   Boolean indicating whether the view is scrollable, as specified during
   creation.
 - **readonly**: [bWindow](./window.md#bwindow)\* `cur_wnd`:
   The window on which this view is being shown or NULL if there is no such
   window.
 - **semi-public readonly**: bWidgetListItem\* `selected`:
   If not NULL, `view->selected->box` is guaranteed to be a pointer
   to the currently focused box widget.

```c
typedef struct {
	/* public */ bEventHandler *keyup, *keydown;
	/* public */ bBoxWidget *body, *bg;
	/* public */ bBoxWidget *header;
	/* public */ bSoftkeyPanel *sk;
	/* readonly */ unsigned int scrollable;
	/* readonly */ bWindow *cur_wnd;
	/* semi-public readonly */ bWidgetListItem *selected;
} bView;
```
