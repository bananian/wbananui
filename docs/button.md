# Button widget API

Defined in: [`<bananui/highlevel.h>`](../include/bananui/highlevel.h)

## Functions

### bCreateButtonWidget

```c
bButtonWidget *bCreateButtonWidget(const char *text, bButtonStyle st,
  bContentAlign align, int width, int height)
```

Argument:
 - const char \* `text`: The button text.
 - [bButtonStyle](#bbuttonstyle): What kind of button to create
   (radio, checkbox, etc.)
 - [bContentAlign](./widget.md#bcontentalign) `align`: Alignment of the
   button relative to its parent box.
 - int `width`: The width of the button, as passed to
   [bCreateBoxWidget](./widget.md#bcreateboxwidget).
 - int `height`: The height of the button, as passed to
   [bCreateBoxWidget](./widget.md#bcreateboxwidget).

Return value: a new [bButtonWidget](#bbuttonwidget)\* or NULL on error.

### bSetButtonDefaultColor

```c
void bSetButtonDefaultColor(bButtonWidget *btn,
  bWidgetColor fg, bWidgetColor bg)
```

Override the theme defaults for this button's non-selected color.

Arguments:
 - [bButtonWidget](#bbuttonwidget)\* `btn`: The button.
 - [bWidgetColor](./widget.md#bwidgetcolor) `fg`: Foreground (text) color.
 - [bWidgetColor](./widget.md#bwidgetcolor) `bg`: Background color.

### bSetButtonSelectedColor

```c
void bSetButtonDefaultColor(bButtonWidget *btn,
  bWidgetColor fg, bWidgetColor bg)
```

Override the theme defaults for this button's selected color.
Useful for marking "dangerous" buttons with a different color.

Arguments:
 - [bButtonWidget](#bbuttonwidget)\* `btn`: The button.
 - [bWidgetColor](./widget.md#bwidgetcolor) `fg`: Foreground (text) color.
 - [bWidgetColor](./widget.md#bwidgetcolor) `bg`: Background color.

### bSetButtonState

```c
void bSetButtonState(bButtonWidget *btn, int state)
```

Update the state of a checkbox or radio button.

Arguments:
 - [bButtonWidget](#bbuttonwidget)\* `btn`: The button.
 - int `state`: Boolean indicating whether the button is checked.

## Enumerations

### bButtonStyle

```c
typedef enum {
	B_BUTTON_STYLE_PUSHBUTTON,
	B_BUTTON_STYLE_MENU,
	B_BUTTON_STYLE_ICON,
	B_BUTTON_STYLE_CHECKBOX,
	B_BUTTON_STYLE_RADIO
} bButtonStyle;
```

## Structs

### bButtonWidget

Constructor: [bCreateButtonWidget](#bcreatebuttonwidget).

Destroy by using [bDestroyBoxRecursive](./widget.md#bdestroyboxrecursive)
on the button's box or any of its parents.

Fields:
 - **readonly**: [bBoxWidget](./widget.md#bboxwidget) `box`:
   The main box of the button. Use this to add the button to another box.
 - **public**: [bContentWidget](./widget.md#bcontentwidget) `icon`:
   Only for icon buttons. Load the button's icon into this widget.
 - **public**: [bContentWidget](./widget.md#bcontentwidget) `cont`:
   Use this to change the button's text.
 - **public**: [bContentWidget](./widget.md#bcontentwidget) `subtitle`:
   Use this to set the button's subtitle.
 - **readonly**: [bWidgetColor](./widget.md#bwidgetcolor)
   `default_fg_color`, `default_bg_color`,
   `selected_fg_color`, `selected_bg_color`:
   Button colors which can be set by
   [bSetButtonDefaultColor](#bsetbuttondefaultcolor) and
   [bSetButtonSelectedColor](#bsetbuttonselectedcolor).
 - **readonly**: [bButtonStyle](#bbuttonstyle) `style`: The button type,
   as specified during creation.
 - **readonly**: int `state`: Boolean indicating whether a checkbox or radio
   button is checked.
 - **readonly**: int `focused`: Boolean indicating whether the button is
   currently focused.

```
typedef struct {
	/* Use these fields to set the button text and icon
	 * (but not the background color!) */
	/* public */ bContentWidget *icon;
	/* public */ bContentWidget *cont;
	/* public */ bContentWidget *subtitle;

	/* readonly */ bBoxWidget *box;
	/* readonly */ bWidgetColor default_fg_color, default_bg_color;
	/* readonly */ bWidgetColor selected_fg_color, selected_bg_color;

	/* readonly */ bButtonStyle style;
	/* readonly */ int state, focused;
} bButtonWidget;
```
