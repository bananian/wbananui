# API docs

Common workflow:
- App structure
  - Create a window
  - Create a view
  - Add widgets to the view (possibly using the simple API)
  - Register focus event handlers
  - Show the view
  - Register other event handlers with an "application state" object as userdata
  - Redraw the window
  - Loop using `poll`/`select` while `appstate->running` and not `window->closed`
- Building
  - Use pkg-config. The package name is `libwbananui1`

## Modules

 - [Event utils](./utils.md)
 - [Widget](./widget.md)
 - [Window](./window.md)
 - [View](./view.md)
 - [Input](./input.md)
 - [Button](./button.md)
 - [Settings](./settings.md)
 - [Simple API](./simple.md)
 - [Keys](./keys.md)

## Thread safety

wbananui is not thread safe. Certain modules store data globally. If you want
to use wbananui in a multi-threaded application, please only make API calls
from one thread or use a global lock for wbananui.
