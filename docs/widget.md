# Widget API

Defined in: [`<bananui/widget.h>`](../include/bananui/widget.h)

## Functions

### bCreateBoxWidget

```c
bBoxWidget *bCreateBoxWidget(bContentAlign align, bBoxDirection dir,
  double width, double height)
```

Arguments:
 - [bContentAlign](#bcontentalign) `align`: The alignment of the box relative
   to the parent.
 - [bBoxDirection](#bboxdirection) `dir`: The direction in which children
   will be stacked (vertical or horizontal).
 - double `width`: If less than zero, stretches the box to the maximum
   available width. If equal to zero, uses as much width as needed for the
   content. Otherwise makes the box `width` pixels wide.
 - double `width`: If less than zero, stretches the box to the maximum
   available height. If equal to zero, uses as much width as needed for the
   content. Otherwise makes the box `height` pixels high.

Return value: [bBoxWidget](#bboxwidget)\* or NULL if out of memory.

### bCreateContentWidget

```c
bContentWidget *bCreateContentWidget(bContentAlign align, bContentFlags flags)
```

Arguments:
 - [bContentAlign](#bcontentalign) `align`: The alignment of the content in
   this widget. Note: specifies both vertical and horizontal alignment.
 - bContentFlags `flags`: Reserved. Please specify 0.

Return value: [bContentWidget](#bcontentwidget)\* or NULL if out of memory.

### bInsertBox

```c
int bInsertBox(bBoxWidget *box, bBoxWidget *before)
```

Inserts `box` at the position of `before`.

Arguments:
 - [bBoxWidget](#bboxwidget) `box`: The box to be inserted.
 - [bBoxWidget](#bboxwidget) `before`: A box at the position where
   the box is to be inserted. Must be attached to a parent box.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bAddBoxToBox

```c
int bAddBoxToBox(bBoxWidget *box, bBoxWidget *child)
```

Adds `child` to the end of `box`.

Arguments:
 - [bBoxWidget](#bboxwidget) `box`: The parent box.
 - [bBoxWidget](#bboxwidget) `child`: The child box.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bAddContentToBox

```c
int bAddContentToBox(bBoxWidget *box, bContentWidget *cont)
```

Adds `cont` to the end of `box`.

Arguments:
 - [bBoxWidget](#bboxwidget) `box`: The parent box.
 - [bContentWidget](#bcontentwidget) `cont`: The content widget.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bAddFillToBox

```c
int bAddFillToBox(bBoxWidget *box)
```

Adds empty space to the end of `box`.
Equivalent to adding an empty box stretched in both directions.

Arguments:
 - [bBoxWidget](#bboxwidget) `box`: The box to add the empty space to.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bSetTextContent

```c
int bSetTextContent(bContentWidget *cont, const char *txt, PangoWeight weight,
  double size)
```

Arguments:
 - [bContentWidget](#bcontentwidget)\* `cont`: The content widget.
 - const char \* `txt`: The text encoded in UTF-8.
 - PangoWeight `weight`: The font weight (100-1000). Pango's weight constants
   can be used here.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bLoadImageFromFile

```c
int bLoadImageFromFile(bContentWidget *cont, const char *filename)
```

Loads an image from a PNG or SVG file. The file must have a .png or .svg
suffix.

Arguments:
 - [bContentWidget](#bcontentwidget)\* `cont`: The content widget.
 - const char \* `filename`: The path to the file.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bLoadImageFromIcon

```c
int bLoadImageFromFile(bContentWidget *cont, const char *name, int size)
```

Loads an image from a stock icon.

Arguments:
 - [bContentWidget](#bcontentwidget)\* `cont`: The content widget.
 - const char \* `name`: The name of the icon. The Adwaita icon theme is
   used by default. Use `yad-icon-browser` or `gtk3-icon-browser` to find
   icons.
 - int `size`: The size of the icon in pixels.

Return value: 0 on error, 1 on success. (FIXME! API inconsistency!)

### bColorFromTheme

```c
bWidgetColor bColorFromTheme(const char *name)
```

Load a color value from a theme. Set [bGlobalAppClass](#bglobalappclass) for
theme variations.

Arguments:
 - const char \* `name`: The name of the color. Common names are `text`,
   `background` and `secondary`.

Return value: a [bWidgetColor](#bwidgetcolor)

### bRGBA

```c
bWidgetColor bRGBA(double r, double g, double b, double a)
```

Construct a color from components.

Arguments:
 - double `r`, `g`, `b`, `a`: The color components.
   Values range from 0.0 to 1.0.

Return value: a [bWidgetColor](#bwidgetcolor)

### bGetRGBA

```c
void bGetRGBA(bWidgetColor *clr, double *r, double *g, double *b, double *a)
```

Get RGBA components from a color.

Arguments:
 - double\* `r`, `g`, `b`, `a`: The destinations for color components.
   May be NULL.

### bComputeLayout

```c
int bComputeLayout(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box);
```

Used to compute the layout (width and height) of a box and its children.

Arguments:
 - cairo\_surface\_t\* `surf`: A Cairo surface used for computing the
   available display dimensions.
 - cairo\_t\* `cr`: A Cairo instance used for computing the
   available display dimensions.
 - [bBoxWidget](#bboxwidget)\* `box`: The box.

Return value: please ignore (FIXME! API inconsistency!)

### bShowMainBox

```c
int bShowMainBox(cairo_surface_t *surf, cairo_t *cr, bBoxWidget *box,
  double top, double left);
```

Used to render a box and its children on a Cairo surface.
Typical applications use the [view](./view.md) API instead of this function.

Arguments:
 - cairo\_surface\_t\* `surf`: The Cairo surface.
 - cairo\_t\* `cr`: A Cairo renderer instance.
 - [bBoxWidget](#bboxwidget)\* `box`: The box.
 - double `top`: Top offset of the box.
 - double `left`: Left offset of the box.

Return value: please ignore (FIXME! API inconsistency!)

### bDestroyBoxRecursive

```c
void bDestroyBoxRecursive(bBoxWidget *box)
```

Destroys a box and all of its children, removing it from a parent
box if necessary.

Arguments:
 - [bBoxWidget](#bboxwidget)\* `box`: The box to destroy.


### bDestroyBoxChildren

```c
void bDestroyBoxChildren(bBoxWidget *box)
```

Clear a box, destroying all its children.

Arguments:
 - [bBoxWidget](#bboxwidget)\* `box`: The box to clear.

## Public globals

### bGlobalAppClass

```c
extern const char *bGlobalAppClass
```

Specifies the class of the app for theme variations. Common values are
`utility`, `phone` and `multimedia`. May be set by the app, but not mandatory.

## Enumerations

### bContentAlign

```c
typedef enum {
	B_ALIGN_START,
	B_ALIGN_CENTER,
	B_ALIGN_END
} bContentAlign;
```

### bContentWrapMode

```c
typedef enum {
	B_WRAP_ELLIPSIZE,
	B_WRAP_BREAK,
	B_WRAP_WORD,
	B_WRAP_ELLIPSIZE_START,
} bContentWrapMode;
```

### bBoxDirection

```c
typedef enum {
	B_BOX_VBOX,
	B_BOX_HBOX
} bBoxDirection;
```

### bContentType

```c
typedef enum {
	B_CONTENT_TYPE_EMPTY,
	B_CONTENT_TYPE_TEXT,
	B_CONTENT_TYPE_IMAGE,
	B_CONTENT_TYPE_IMAGE_SVG
} bContentType;
```

## Structs

### bBoxWidget

Constructor: [bCreateBoxWidget](#bcreateboxwidget)

Fields:
 - **public**: [bWidgetColor](#bwidgetcolor) `color`:
   The background color of the box. Default is transparent (#00000000).
 - **public**: double `mgn_l`, `mgn_t`, `mgn_b`, `mgn_r`:
   Left, top, bottom and right box margins. Zero by default.
 - **public**: unsigned int `focusable`:
   Boolean indicating whether the box is part of the navigation flow and
   `focusin` and `focusout` should be called.
 - **public**: unsigned int `clip`:
   Boolean indicating whether content outside the limits of the box should
   be clipped.
 - **readonly**: [bComputedLayout](#bcomputedlayout) `computed`:
   The computed size and position of the widget.
 - **readonly**: [bBoxDirection](#bboxdirection) `dir`:
   The flow direction of the box, as specified during creation.
 - **readonly**: [bContentAlign](#bcontentalign) `align`:
   The alignment of the box, as specified during creation.
 - **readonly**: double `width`, `height`:
   The width and height of the box, as specified during creation.

Events:
 - `focusin`: Called when the user navigates to this widget.
 - `focusout`: Called when the user navigates to a different
   widget.
 - `click`: Called when the user presses the enter key while this widget
   is focused.
 - `skright`: Called when the user presses the right soft key while this
   widget is focused.
 - `skleft`: Called when the user presses the left soft key while this
   widget is focused.

```c
typedef struct {
	/* readonly */ bBoxDirection dir;
	/* public */ bWidgetColor color;
	/* readonly */ bContentAlign align;
	/* readonly */ bComputedLayout computed;

	/* <0 means stretch, =0 means use minimum needed space */
	/* readonly */ double width, height;

	/* public */ double mgn_l, mgn_t, mgn_r, mgn_b;
	/* public */ unsigned int focusable, clip;
	/* public */ bEventHandler *focusin, *focusout, *click, *skright,
		*skleft;
} bBoxWidget;
```

### bContentWidget

Constructor: [bCreateContentWidget](#bcreatecontentwidget)

Fields:
 - **public**: int `cursor`: The position of the cursor as a _byte offset_.
   When set to -1 (default), text will not have a cursor and not scroll.
 - **public**: int `cursor_visible`: Boolean indicating whether the cursor
   should be visible. Has no effect on text layout.
 - **public**: [bContentWrapMode](#bcontentwrapmode) `wrapmode`: Wrapping mode
   for text in this widget. Set to `B_WRAP_WORD` by default.
 - **public**: [bWidgetColor](#bwidgetcolor) `color`: The color for text or
   symbolic icons.
 - **readonly**: [bComputedLayout](#bcomputedlayout) `computed`:
   The computed size and position of the widget.
 - **readonly**: [bContentAlign](#bcontentalign) `align`:
   The alignment of the content, as specified during creation.
 - **readonly**: bContentFlags `flags`: Reserved.
 - **readonly**: [bContentType](#bcontenttype) `type`:
   The content type. Depends on which functions were called to set the content
   and which data format was detected (for images).

```c
typedef struct {
	/* public */ int cursor, cursor_visible;
	/* readonly */ bContentAlign align;
	/* readonly */ bContentFlags flags;
	/* readonly */ bContentType type;
	/* public */ bContentWrapMode wrapmode;
	/* public */ bWidgetColor color;
	/* readonly */ bComputedLayout computed;
} bContentWidget;
```

### bWidgetColor

Constructor: [bRGBA](#brgba) or [bColorFromTheme](#bcolorfromtheme).

Initializer: `B_RGBA_INITIALIZER(r, g, b, a)`

Private structure.
