# Utils API

Defined in: [`<bananui/utils.h>`](../include/bananui/utils.h)

## Functions

### bClearEvent

```c
void bClearEvent(bEventHandler **event)
```

Remove all listeners from an event, freeing all associated resources.

### bRegisterEventHandler

```c
void bRegisterEventHandler(bEventHandler **event,
  bEventCallback cb, void *userdata)
```

Add a listener for an event. This listener will be called before
the previously registered listeners.

Arguments:
 - [bEventHandler](#beventhandler)\*\* `event`: A pointer to the event field
   (handler list).
 - [bEventCallback](#beventcallback) `cb`: The function to be called when
   the event is triggered.
 - void\* `userdata`: Some data to be passed to the event.

### bUnregisterEventHandler

```c
void bUnregisterEventHandler(bEventHandler **event,
  bEventCallback cb, void *userdata)
```

Remove a listener for an event, freeing the associated resources.

Arguments: the same as passed to [bRegisterEventHandler](#bregistereventhandler)

### bTriggerEvent

```c
int bTriggerEvent(bEventHandler *event, void *param)
```

Trigger all listeners for an event, starting with the last-added listener.

Arguments:
 - [bEventHandler](#beventhandler)\* `event`: The event handler list itself
   (not a double-pointer!)
 - void\* `param`: A value to be passed to the listeners.

Return value: 0 if the event processing was aborted by an event listener,
1 if all event listeners returned nonzero values.

## Callbacks

### bEventCallback

```c
typedef int (*bEventCallback)(void *param, void *userdata)
```

Arguments:
 - void\* `param`: An event-specific value.
 - void\* `userdata`: User-specified data for the listener.

Return value: 0 if the event processing should be aborted (preventing the
invocation of previously-assigned listeners), nonzero if the processing
should continue.

**WARNING**: Always return zero after calling bUnregisterEventHandler in
an event handler! If the modified event happens to be related to the
event being handled (e.g., when unregistering a keydown handler from
inside a click handler), returning a nonzero value will cause unpredictable
behavior in the event loop.

## Structs

### bEventHandler

A list of event handlers which also represents the event itself.

Private structure.
