# Settings API

**NOTE:** This is a very rudimentary settings API, mostly used for
reading device-specific constants like the device name or keymaps.
Applications that need a more advanced settings API can use glib's
`GSettings`.

Defined in: [`<bananui/settings.h>`](../include/bananui/settings.h)

## Functions

### bCreateSettingsManager

```c
bSettings *bCreateSettingsManager(void)
```

Create a settings manager. Settings are **not synchronized** between manager
instances, so it is recommended to create only one settings manager
per application.

Return value: a new [bSettings](#bsettings)\* or NULL on error.

### bDestroySettingsManager

```c
void bDestroySettingsManager(bSettings *mgr)
```

Arguments:
 - [bSettings](#bsettings)\* `mgr`: The manager instance to destroy.
   If NULL, nothing is done.

### bGetSettingInt

```c
int bGetSettingInt(bSettings *mgr, const char *name, int defval)
```

Arguments:
 - [bSettings](#bsettings)\* `mgr`: The settings manager. If NULL, defval
   will be returned.
 - const char \* `name`: The setting name to get. See the [list](#list)
 - int `defval`: The default value to return.

Return value: the setting value or `defval` on error.

### bGetSettingString

```c
char *bGetSettingString(bSettings *mgr, const char *name, const char *defval)
```

Arguments:
 - [bSettings](#bsettings)\* `mgr`: The settings manager. If NULL,
   a copy of defval will be returned.
 - const char \* `name`: The setting name to get. See the [list](#list)
 - const char \* `defval`: The default value to return.

Return value: the setting value, a copy of `defval` on error or NULL when
out of memory. The result must be freed.

### bSetSettingInt

```c
int bSetSettingInt(bSettings *mgr, const char *name, int val)
```

Arguments:
 - [bSettings](#bsettings)\* `mgr`: The settings manager. If NULL, -1 is
   returned.
 - const char \* `name`: The setting name to get. See the [list](#list)
 - int `val`: The value to set.

Return value: -1 on error, 0 on success

### bSetSettingString

```c
int bSetSettingString(bSettings *mgr, const char *name, const char *val)
```

Arguments:
 - [bSettings](#bsettings)\* `mgr`: The settings manager. If NULL, -1 is
   returned.
 - const char \* `name`: The setting name to get. See the [list](#list)
 - const char \* `val`: The value to set.

Return value: -1 on error, 0 on success

## Structs

### bSettings

Constructor: [bCreateSettingsManager](#bcreatesettingsmanager).

Private structure.

## List

This list is incomplete. Boolean settings are represented using `1` and `0`.

### Device settings

 - `device.brand`: The brand of the device (e.g., Nokia)
 - `device.name`: The full name of the device (e.g., Nokia 8110 4G)
 - `input.keys.endcall.present`: Whether the device has separate end-call and
   back buttons.
 - `input.keys.shortcut.present`: Whether the device has a dedicated
   shortcut key (message key).
 - `input.keys.assistant.present`: Whether the device has an assistant or
   SOS key
 - `input.keys.power.present`: Whether the device has a dedicated
   power key
 - `input.keys.lid.type`:
   - `flip`: Closed/Open switch present
   - `slide`: Closed/Open/FullOpen switches present
   - `none`: Phone has no flip/slide

### User settings

Feel free to invent your own names! Please prefix them application's name
to avoid naming collisions. Note that apps can modify other apps' settings.
Store your settings somewhere else if they need protection from other apps.

Device settings cannot be overridden for security reasons.
