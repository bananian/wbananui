# Non-standard keysym constants

Defined in: [`<bananui/keys.h>`](../include/bananui/keys.h)

## Available keys:

- `BANANUI_KEY_SoftLeft`: LSK
- `BANANUI_KEY_SoftRight`: RSK
- `BANANUI_KEY_Star`: \*
- `BANANUI_KEY_Pound`: #
- `BANANUI_KEY_Call`
- `BANANUI_KEY_EndCall` (alias for `XKB_KEY_Escape`)
- `BANANUI_KEY_Back` (alias for `XKB_KEY_BackSpace`)
- `BANANUI_KEY_Shortcut`
- `BANANUI_KEY_Assistant`
- `BANANUI_KEY_LidClosed`
- `BANANUI_KEY_LidOpened`
- `BANANUI_KEY_LidFullOpened` (slide only)
