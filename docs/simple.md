# Simple API

The simple wbananui API is designed in a more functional manner than the
low-level object-oriented API: widgets returned by function calls are passed
as arguments to other widget-returning function calls.

It is designed for creating a static initial GUI structure that may be modified
or filled later by other [widget API functions](./widget.md#functions).

## Functions

All functions return [sbItem](#sbitem)s.
Errors are propagated from children to their parents.

### Widgets

- sbBOX(align, dir, width, height, ...attrs, ...children, sbEND)
- sbTEXT(align, txt, weight, size, ...attrs, sbEND)
- sbIMAGE(align, filename, ...attrs, sbEND)
- sbICON(align, name, size, ...attrs, sbEND)

### Attributes

- sbRGBA(double r, double g, double b, double a)
- sbMGNL(double margin)
- sbMGNR(double margin)
- sbMGNT(double margin)
- sbMGNB(double margin)
- sbREF(struct sbItem \*ref) - saves a copy of the widget's sbItem to \*ref

## Enumerations

### sbItemType

```c
enum sbItemType {
	SB_ITEM_END,
	SB_ITEM_BOX,
	SB_ITEM_CONTENT,
	SB_ITEM_COLOR,
	SB_ITEM_REF,
	SB_ITEM_MGN_L,
	SB_ITEM_MGN_R,
	SB_ITEM_MGN_T,
	SB_ITEM_MGN_B,
	SB_ITEM_ERROR
};
```

## Structs

### sbItem

```c
struct sbItem {
	enum sbItemType type;
	union {
		bBoxWidget *box; /* SB_ITEM_BOX */
		bContentWidget *cont; /* SB_ITEM_CONTENT */
		bWidgetColor color; /* SB_ITEM_COLOR */
		struct sbItem *ref; /* SB_ITEM_REF */
		const char *err; /* SB_ITEM_ERROR */
		double margin; /* SB_ITEM_MGN_* */
	};
};
```

## [Example](../apps/uitest/simple.c)
