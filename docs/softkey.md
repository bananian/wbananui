# Softkey API

Defined in: [`<bananui/softkey.h>`](../include/bananui/softkey.h)

## Functions

### bCreateSoftkeyPanel

```c
bSoftkeyPanel *bCreateSoftkeyPanel(void)
```

Return value: a new [bSoftkeyPanel](#bsoftkeypanel)\* or NULL on error.

### bDestroySoftkeyPanel

```c
void bDestroySoftkeyPanel(bSoftkeyPanel *sk)
```

Arguments:
 - [bSoftkeyPanel](#bsoftkeypanel)\* `sk`: The panel to destroy.

### bSetSoftkeyColor

```c
void bSetSoftkeyColor(bSoftkeyPanel *sk, bWidgetColor fg, bWidgetColor bg)
```

Override the color theme for a softkey panel.

Arguments:
 - [bSoftkeyPanel](#bsoftkeypanel)\* `sk`: The softkey panel.
 - [bWidgetColor](./widget.md#bwidgetcolor) `fg`: The text color.
 - [bWidgetColor](./widget.md#bwidgetcolor) `bg`: The background color.

### bSetSoftkeyText

```c
void bSetSoftkeyText(bSoftkeyPanel *sk,
  const char *l, const char *c, const char *r)
```

If an argument is NULL, the corresponding label will not be replaced.

Arguments:
 - const char \* `l`: Left softkey label.
 - const char \* `c`: Center softkey label.
 - const char \* `r`: Right softkey label.

### bShowSoftkeyPanel

```c
int bShowSoftkeyPanel(cairo_surface_t *surf, cairo_t *cr, bSoftkeyPanel *sk)
```

Render the softkey panel to a Cairo surface.

Arguments:
 - cairo\_surface\_t\* `surf`: The Cairo surface.
 - cairo\_t\* `cr`: A Cairo renderer instance.
 - [bSoftkeyPanel](#bsoftkeypanel)\* `sk`: The softkey panel.

Return value: please ignore (FIXME! API inconsistency!)

## Structs

### bSoftkeyPanel

Constructor: [bCreateSoftkeyPanel](#bcreatesoftkeypanel).

Private structure.
