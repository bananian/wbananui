/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <bananui/utils.h>
#include <bananui/widget.h>
#include <bananui/softkey.h>

bSoftkeyPanel *bCreateSoftkeyPanel(void)
{
	bSoftkeyPanel *panel;
	bWidgetColor fg, bg;
	panel = malloc(sizeof(bSoftkeyPanel));
	if(!panel) return NULL;
	panel->main = NULL;
	panel->lbox = panel->cbox = panel->rbox = NULL;
	panel->lcont = panel->ccont = panel->rcont = NULL;
	panel->main = bCreateBoxWidget(B_ALIGN_START, B_BOX_HBOX, -1,
		SOFTKEY_HEIGHT);
	if(!panel->main) goto free_panel;

	panel->lbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0);
	if(!panel->lbox) goto err;
	bAddBoxToBox(panel->main, panel->lbox);

	panel->cbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 0, 0);
	if(!panel->cbox) goto err;
	bAddBoxToBox(panel->main, panel->cbox);

	panel->rbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, -1, 0);
	if(!panel->rbox) goto err;
	bAddBoxToBox(panel->main, panel->rbox);

	panel->lcont = bCreateContentWidget(B_ALIGN_START, 0);
	if(!panel->lcont) goto err;
	bAddContentToBox(panel->lbox, panel->lcont);

	panel->ccont = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!panel->ccont) goto err;
	bAddContentToBox(panel->cbox, panel->ccont);

	panel->rcont = bCreateContentWidget(B_ALIGN_END, 0);
	if(!panel->rcont) goto err;
	bAddContentToBox(panel->rbox, panel->rcont);

	panel->lbox->mgn_l = 5;
	panel->rbox->mgn_r = 5;
	bSetSoftkeyText(panel, "", "", "");
	fg = bColorFromTheme("softkey:text");
	bg = bColorFromTheme("softkey:background");
	bSetSoftkeyColor(panel, fg, bg);
	return panel;
err:
	bDestroyBoxRecursive(panel->main);
free_panel:
	free(panel);
	return NULL;
}

void bSetSoftkeyColor(bSoftkeyPanel *sk, bWidgetColor fg, bWidgetColor bg)
{
	sk->main->color = bg;
	sk->lcont->color = fg;
	sk->ccont->color = fg;
	sk->rcont->color = fg;
}

void bSetSoftkeyText(bSoftkeyPanel *sk,
	const char *l, const char *c, const char *r)
{
	if(l) bSetTextContent(sk->lcont, l, PANGO_WEIGHT_NORMAL, 14);
	if(c) bSetTextContent(sk->ccont, c, PANGO_WEIGHT_BOLD, 17);
	if(r) bSetTextContent(sk->rcont, r, PANGO_WEIGHT_NORMAL, 14);
}

int bShowSoftkeyPanel(cairo_surface_t *surf, cairo_t *cr, bSoftkeyPanel *sk)
{
	return bShowMainBox(surf, cr, sk->main,
		auto_surface_get_height(surf) - SOFTKEY_HEIGHT, 0);
}

void bDestroySoftkeyPanel(bSoftkeyPanel *sk)
{
	if(!sk) return;
	bDestroyBoxRecursive(sk->main);
	free(sk);
}
