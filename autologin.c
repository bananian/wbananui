#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <grp.h>
#include <pwd.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_misc.h>

static int conversation(int num_msg, const struct pam_message **msg,
		struct pam_response **resp, void *appdata)
{
	assert(0 && "Unexpected interactive communication");
}

static void terminate_error(pam_handle_t *pamhan, int result)
{
	fprintf(stderr, "PAM Error: %s\n", pam_strerror(pamhan, result));
	pam_end(pamhan, result);
	exit(1);
}

int main(void)
{
	int pid, result;
	pam_handle_t *pamhan;
	struct pam_conv conv;
	struct passwd *passent;
	/* This program does not authenticate the user, its main job
	 * is to set up a working logind session
	 */

	conv.conv = conversation;
	conv.appdata_ptr = NULL;

	result = pam_start("bananui-autologin", "user", &conv, &pamhan);
	if(result != PAM_SUCCESS) terminate_error(pamhan, result);

	pam_putenv(pamhan, "XDG_VTNR=1");
	pam_putenv(pamhan, "XDG_SEAT=seat0");
	pam_putenv(pamhan, "USER=user");
	pam_putenv(pamhan, "LOGNAME=user");

	result = pam_setcred(pamhan, PAM_ESTABLISH_CRED);
	if(result != PAM_SUCCESS) terminate_error(pamhan, result);

	result = pam_open_session(pamhan, 0);
	if(result != PAM_SUCCESS) terminate_error(pamhan, result);

	passent = getpwnam("user");
	pam_misc_setenv(pamhan, "HOME", passent->pw_dir, 0);
	pam_misc_setenv(pamhan, "SHELL", passent->pw_shell, 0);
	pam_misc_setenv(pamhan, "PATH", "/usr/local/bin:/usr/bin:/bin", 0);


	if((pid = fork()) == 0){
		if(initgroups("user", passent->pw_gid)){
			perror("initgroups(user)");
			_exit(1);
		}
		if(setgid(passent->pw_gid)){
			perror("setgid(user)");
			_exit(1);
		}
		if(setuid(passent->pw_uid)){
			perror("setuid(user)");
			_exit(1);
		}
		setsid();
		if(chdir(passent->pw_dir) < 0){
			perror(passent->pw_dir);
			chdir("/");
		}
		execle("/usr/bin/bananui-compositor", "bananui-compositor",
			(char *) NULL, pam_getenvlist(pamhan));
		perror("/usr/bin/bananui-compositor");
		_exit(1);
	}
	if(pid > 0) waitpid(pid, NULL, 0);
	else perror("fork");
	pam_close_session(pamhan, 0);
	pam_setcred(pamhan, PAM_DELETE_CRED);
	pam_end(pamhan, PAM_SUCCESS);
	return 1;
}
