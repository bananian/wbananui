/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unistd.h>
#include <stdio.h>
#include <gmenu-tree.h>
#include <gio/gdesktopappinfo.h>
#include <bananui/widget.h>
#include <bananui/softkey.h>
#include "topbar.h"
#include "homescreen.h"

#define APPLIST_SIZE_FACTOR 0.8
#define APPLIST_TITLE_HEIGHT 30

struct app_list_item {
	GDesktopAppInfo *appinfo;
	bBoxWidget *app, *page;
	bContentWidget *icon;
	char *name;
	unsigned int x, y;
	struct app_list_item *next, *prev;
};

struct homescreen_data {
	cairo_surface_t *surf, *al_surf;
	cairo_t *cr, *al_cr;
	unsigned char *pixels, *al_pixels;
	unsigned int height, width, stride, al_gridsize;
	struct wlr_texture *texture, *al_texture;
	struct wlr_renderer *renderer;
	bBoxWidget *pad1, *mainbox;
	bContentWidget *clock, *welcome;
	bBoxWidget *apptitle, *al_mainbox;
	bContentWidget *titletext;
	bSoftkeyPanel *sk, *al_sk;
	struct app_list_item *applist, *focus, *applist_last;
	float applist_alpha;
	int applist_dalpha;
};

static void homescreen_repaint_into_texture(struct homescreen_data *hs)
{
	/* App list */
	cairo_save(hs->al_cr);
	cairo_set_source_rgba(hs->al_cr, 0.0, 0.0, 0.0, 0.75);
	cairo_set_operator(hs->al_cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint(hs->al_cr);
	cairo_restore(hs->al_cr);
	bShowMainBox(hs->al_surf, hs->al_cr, hs->al_mainbox, 0, 0);
	if(hs->focus){
		bShowMainBox(hs->al_surf, hs->al_cr, hs->focus->page,
			hs->height / 2 - hs->al_gridsize / 2 +
			TOPBAR_HEIGHT / 2, 0);
	}
	bShowSoftkeyPanel(hs->al_surf, hs->al_cr, hs->al_sk);
	/* Clock screen */
	cairo_save(hs->cr);
	cairo_set_operator(hs->cr, CAIRO_OPERATOR_CLEAR);
	cairo_paint(hs->cr);
	cairo_restore(hs->cr);
	bShowMainBox(hs->surf, hs->cr, hs->mainbox, 0, 0);
	bShowSoftkeyPanel(hs->surf, hs->cr, hs->sk);
	if(hs->texture) wlr_texture_destroy(hs->texture);
	hs->texture = wlr_texture_from_pixels(hs->renderer,
		WL_SHM_FORMAT_ARGB8888,
		hs->stride, hs->width, hs->height, hs->pixels);
	if(hs->al_texture) wlr_texture_destroy(hs->al_texture);
	hs->al_texture = wlr_texture_from_pixels(hs->renderer,
		WL_SHM_FORMAT_ARGB8888,
		hs->stride, hs->width, hs->height, hs->al_pixels);
}


static void recursively_store_apps(struct homescreen_data *hs,
	GMenuTreeIter *iter)
{
	GMenuTreeIter *subiter;
	GMenuTreeDirectory *subdir;
	GMenuTreeEntry *entry;
	struct app_list_item **item = hs->applist_last ?
		&(hs->applist_last->next) : &(hs->applist);
	while(1){
		switch(gmenu_tree_iter_next(iter)){
			case GMENU_TREE_ITEM_INVALID:
				return;
			case GMENU_TREE_ITEM_DIRECTORY:
				subdir = gmenu_tree_iter_get_directory(iter);
				subiter = gmenu_tree_directory_iter(subdir);
				recursively_store_apps(hs, subiter);
				gmenu_tree_iter_unref(subiter);
				break;
			case GMENU_TREE_ITEM_ENTRY:
				entry = gmenu_tree_iter_get_entry(iter);
				if(*item) item = &((*item)->next);
				*item = malloc(sizeof(struct app_list_item));
				if(!*item) return;
				(*item)->appinfo =
					gmenu_tree_entry_get_app_info(entry);
				(*item)->name = g_desktop_app_info_get_string(
						(*item)->appinfo, "Name");
				(*item)->x = (*item)->y = 0;
				(*item)->next = NULL;
				(*item)->app = NULL;
				(*item)->page = NULL;
				(*item)->prev = hs->applist_last;
				hs->applist_last = *item;
				break;
			default:
				break;
		}
	}
}

static void homescreen_add_apps(struct homescreen_data *hs)
{
	struct app_list_item *tmp;
	int i, j;
	GMenuTree *menu;
	GMenuTreeDirectory *root;
	GMenuTreeIter *iter;
	GError *err = NULL;
	bBoxWidget *page = NULL, *apps = NULL;
	menu = gmenu_tree_new("bananui-applications.menu",
			GMENU_TREE_FLAGS_NONE);
	if(!gmenu_tree_load_sync(menu, &err)){
		if(err)
			fprintf(stderr, "[AL] Failed to load menu: %s",
				err->message);
		return;
	}
	root = gmenu_tree_get_root_directory(menu);
	iter = gmenu_tree_directory_iter(root);
	recursively_store_apps(hs, iter);
	gmenu_tree_iter_unref(iter);
	for(tmp = hs->applist, i = 0; tmp; i++){
		bBoxWidget *line;
		if((i * 3) % 9 == 0){
			page = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX,
						-1, hs->al_gridsize);
			apps = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX,
					hs->al_gridsize, hs->al_gridsize);
			bAddBoxToBox(page, apps);
		}
		line = bCreateBoxWidget(B_ALIGN_START, B_BOX_HBOX,
			hs->al_gridsize, hs->al_gridsize / 3);
		if(!line) return;
		bAddBoxToBox(apps, line);
		for(j = 0; j < 3 && tmp; j++, tmp=tmp->next){
			GIcon *gicon;
			bBoxWidget *app, *iconbox;
			bContentWidget *icon;
			app = bCreateBoxWidget(B_ALIGN_START, B_BOX_VBOX,
				hs->al_gridsize / 3, hs->al_gridsize / 3);
			if(!app) return;
			bAddBoxToBox(line, app);
			iconbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
				0, -1);
			if(!iconbox) return;
			bAddBoxToBox(app, iconbox);
			icon = bCreateContentWidget(B_ALIGN_CENTER, 0);
			if(!icon) return;
			gicon = g_app_info_get_icon(G_APP_INFO(tmp->appinfo));
			if(G_IS_FILE_ICON(gicon)){
				char *path;
				path = g_file_get_path(g_file_icon_get_file(
							G_FILE_ICON(gicon)));
				if(!path || !bLoadImageFromIcon(icon, path, 56))
				{
					bLoadImageFromIcon(icon,
						"image-missing", 56);
				}
				if(path) g_free(path);
			}
			else if(G_IS_THEMED_ICON(gicon)){
				const gchar * const *icons;
				icons = g_themed_icon_get_names(
					G_THEMED_ICON(gicon));
				for(; *icons; icons++){
					if(bLoadImageFromIcon(icon, *icons, 56))
						break;
				}
			}
			bAddContentToBox(iconbox, icon);
			icon->color = bRGBA(0.9, 0.9, 0.9, 1.0);
			tmp->app = app;
			tmp->icon = icon;
			tmp->x = j;
			tmp->y = i;
			tmp->page = page;
		}
	}
}

static void homescreen_focus_app(struct homescreen_data *hs,
	struct app_list_item *app)
{
	bSetTextContent(hs->titletext, app->name, PANGO_WEIGHT_SEMIBOLD, 18);
	if(hs->focus){
		(hs->focus->app->color) = bRGBA(0.0, 0.0, 0.0, 0.0);
		(hs->focus->icon->color) = bRGBA(0.9, 0.9, 0.9, 1.0);
	}
	(app->app->color) = bRGBA(1.0 / app->x, 1.0 / app->y, 0.0, 1.0);
	(app->icon->color) = bRGBA(0.0, 0.0, 0.0, 1.0);
	hs->focus = app;
}

static void homescreen_focus_find(struct homescreen_data *hs,
	int direction)
{
	struct app_list_item *tmp, *fallback = NULL;
	for(tmp = hs->focus; tmp; tmp = (direction < 0) ? tmp->prev : tmp->next)
	{
		if(tmp->y != hs->focus->y) break;
	}
	if(!tmp){
		if(direction > 0) tmp = hs->applist;
		else if(direction < 0){
			fallback = tmp = hs->applist_last;
		}
	}

	for(; tmp; tmp = (direction < 0) ? tmp->prev : tmp->next)
	{
		if(direction > 0) fallback = tmp;
		if(fallback && tmp->y != fallback->y){
			tmp = NULL;
			break;
		}
		if(tmp->x == hs->focus->x) break;
	}
	if(tmp) homescreen_focus_app(hs, tmp);
	else if(fallback) homescreen_focus_app(hs, fallback);
}

struct homescreen_data *init_homescreen(unsigned int width,
	unsigned int height, struct wlr_renderer *renderer)
{
	struct homescreen_data *hs;
	bWidgetColor skfg, skbg;
	hs = calloc(1, sizeof(struct homescreen_data));
	if(!hs) return NULL;
	hs->renderer = renderer;
	hs->texture = NULL;
	hs->al_texture = NULL;
	hs->applist_alpha = 0.0f;
	hs->applist_dalpha = 0;
	hs->width = width;
	hs->height = height;
	hs->applist = hs->applist_last = NULL;
	hs->focus = NULL;
	hs->stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	hs->pixels = calloc(1, hs->stride * height);
	if(!hs->pixels) return NULL;
	hs->al_pixels = calloc(1, hs->stride * height);
	if(!hs->al_pixels){
		free(hs->pixels);
		return NULL;
	}
	hs->surf = cairo_image_surface_create_for_data(hs->pixels,
		CAIRO_FORMAT_ARGB32, width, height, hs->stride);
	if(!hs->surf){
		free(hs->al_pixels);
		free(hs->pixels);
		return NULL;
	}
	hs->al_surf = cairo_image_surface_create_for_data(hs->al_pixels,
		CAIRO_FORMAT_ARGB32, width, height, hs->stride);
	if(!hs->al_surf){
		cairo_surface_destroy(hs->surf);
		free(hs->al_pixels);
		free(hs->pixels);
	}
	hs->cr = cairo_create(hs->surf);
	if(!hs->cr){
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	hs->al_cr = cairo_create(hs->al_surf);
	if(!hs->al_cr){
		cairo_destroy(hs->cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	hs->mainbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX, width, height);
	if(!hs->mainbox){
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	hs->pad1 = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, 0, 40);
	if(!hs->pad1){
		bDestroyBoxRecursive(hs->mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddBoxToBox(hs->mainbox, hs->pad1);
	hs->clock = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!hs->clock){
		bDestroyBoxRecursive(hs->mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddContentToBox(hs->mainbox, hs->clock);
	hs->welcome = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!hs->welcome){
		bDestroyBoxRecursive(hs->mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddContentToBox(hs->mainbox, hs->welcome);
	hs->sk = bCreateSoftkeyPanel();
	if(!hs->sk){
		bDestroyBoxRecursive(hs->mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddContentToBox(hs->mainbox, hs->welcome);
	hs->al_mainbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_VBOX, width, height);
	if(!hs->al_mainbox){
		bDestroyBoxRecursive(hs->mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	hs->apptitle = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX, -1,
			APPLIST_TITLE_HEIGHT);
	if(!hs->apptitle){
		bDestroyBoxRecursive(hs->mainbox);
		bDestroyBoxRecursive(hs->al_mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddBoxToBox(hs->al_mainbox, hs->apptitle);
	hs->titletext = bCreateContentWidget(B_ALIGN_CENTER, 0);
	if(!hs->titletext){
		bDestroyBoxRecursive(hs->mainbox);
		bDestroyBoxRecursive(hs->al_mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	bAddContentToBox(hs->al_mainbox, hs->titletext);
	hs->al_gridsize = width > height ?
			height * APPLIST_SIZE_FACTOR :
			width * APPLIST_SIZE_FACTOR;
	homescreen_add_apps(hs);
	hs->al_sk = bCreateSoftkeyPanel();
	if(!hs->al_sk){
		bDestroyBoxRecursive(hs->mainbox);
		bDestroyBoxRecursive(hs->al_mainbox);
		cairo_destroy(hs->cr);
		cairo_destroy(hs->al_cr);
		cairo_surface_destroy(hs->surf);
		cairo_surface_destroy(hs->al_surf);
		free(hs->pixels);
		free(hs->al_pixels);
		return NULL;
	}
	skbg = bRGBA(0.0, 0.0, 0.0, 0.0);
	skfg = bRGBA(1.0, 1.0, 1.0, 1.0);
	bSetSoftkeyText(hs->sk, "Log out", "APPS", "");
	bSetSoftkeyColor(hs->sk, skfg, skbg);
	bSetSoftkeyText(hs->al_sk, "", "OPEN", "Options");
	bSetSoftkeyColor(hs->al_sk, skfg, skbg);
	hs->clock->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	hs->welcome->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	hs->titletext->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	if(hs->applist){
		homescreen_focus_app(hs, hs->applist);
	}
	bSetTextContent(hs->clock, "00:00", PANGO_WEIGHT_HEAVY, 62);
	bSetTextContent(hs->welcome, "Welcome to Bananian!",
		PANGO_WEIGHT_NORMAL, 17);
	return hs;
}

void homescreen_set_clock(struct homescreen_data *hs, const char *str)
{
	bSetTextContent(hs->clock, str, PANGO_WEIGHT_SEMIBOLD, 62);
	homescreen_repaint_into_texture(hs);
}

enum homescreen_action homescreen_handle_key(struct homescreen_data *hs,
	xkb_keysym_t key)
{
	if(hs->applist_alpha == 0 && hs->applist_dalpha == 0) switch(key){
		case XKB_KEY_Return:
			hs->applist_dalpha = 1;
			if(hs->applist){
				homescreen_focus_app(hs, hs->applist);
				homescreen_repaint_into_texture(hs);
			}
			break;
		default:
			return HS_IGNORE;
	}
	if(hs->applist_alpha == 1 && hs->applist_dalpha == 0) switch(key){
		case XKB_KEY_Return:
			if(hs->focus){
				GError *err = NULL;
				if(!g_app_info_launch(
					G_APP_INFO(hs->focus->appinfo),
					NULL, NULL, &err))
				{
					if(err){
						fprintf(stderr,
							"[AL] Error: %s\n",
							err->message);
					}
				}
				return HS_LAUNCHING;
			}
			break;
		case XKB_KEY_BackSpace:
			hs->applist_dalpha = -1;
			break;
		case XKB_KEY_Left:
			if(hs->focus && hs->focus->prev)
				homescreen_focus_app(hs, hs->focus->prev);
			else if(hs->applist_last)
				homescreen_focus_app(hs, hs->applist_last);
			homescreen_repaint_into_texture(hs);
			break;
		case XKB_KEY_Right:
			if(hs->focus && hs->focus->next)
				homescreen_focus_app(hs, hs->focus->next);
			else if(hs->applist)
				homescreen_focus_app(hs, hs->applist);
			homescreen_repaint_into_texture(hs);
			break;
		case XKB_KEY_Down:
			if(hs->focus)
				homescreen_focus_find(hs, 1);
			homescreen_repaint_into_texture(hs);
			break;
		case XKB_KEY_Up:
			if(hs->focus)
				homescreen_focus_find(hs, -1);
			homescreen_repaint_into_texture(hs);
			break;
		default:
			return HS_IGNORE;
	}
	return HS_RENDER;
}

struct wlr_texture *homescreen_get_texture(struct homescreen_data *hs)
{
	return hs->texture;
}
struct wlr_texture *homescreen_get_applist_texture(struct homescreen_data *hs)
{
	return hs->al_texture;
}
bool homescreen_damage(struct homescreen_data *hs)
{
	return hs->applist_dalpha != 0;
}
double homescreen_next_alpha(struct homescreen_data *hs, long time)
{
	if(hs->applist_dalpha < 0){
		if(hs->applist_alpha == 1.0f){
			return hs->applist_alpha = 0.9f;
		}
		hs->applist_alpha -= time / 200000.0;
		if(hs->applist_alpha <= 0.0f){
			hs->applist_dalpha = 0;
			hs->applist_alpha = 0.0f;
		}
	}
	else if(hs->applist_dalpha > 0){
		if(hs->applist_alpha == 0.0f){
			return hs->applist_alpha = 0.1f;
		}
		hs->applist_alpha += time / 200000.0;
		if(hs->applist_alpha >= 1.0f){
			hs->applist_dalpha = 0;
			hs->applist_alpha = 1.0f;
		}
	}
	return hs->applist_alpha;
}
