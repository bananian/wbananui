/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_TOPBAR_H_
#define _COMP_TOPBAR_H_

#define TOPBAR_HEIGHT 25

struct wlr_renderer;
struct wlr_texture;

struct topbar_data;

struct topbar_data *init_topbar(unsigned int width, unsigned int height,
		struct wlr_renderer *renderer);
struct wlr_texture *topbar_get_texture(struct topbar_data *tb);
void topbar_set_clock(struct topbar_data *tb, const char *time);
void topbar_set_battery(struct topbar_data *tb, int percentage);
void topbar_set_input_method(struct topbar_data *tb, const char *im);
void topbar_set_color_default(struct topbar_data *tb);
void topbar_set_color(struct topbar_data *tb, double r, double g, double b,
		double a);

#endif
