/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <linux/input-event-codes.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <systemd/sd-bus.h>
#include <bananui/settings.h>
#include "deviceinfo.h"

#ifndef KEY_SOS
#define KEY_SOS 0x27f
#endif


struct device_info {
	char *power_button_name, *lid_device_name, *lid2_device_name;
	unsigned int enter_button_keycode;
	unsigned int power_button_keycode;
	unsigned int back_button_keycode;
	unsigned int endcall_button_keycode;
	unsigned int call_button_keycode;
	unsigned int shortcut_button_keycode;
	unsigned int assistant_button_keycode;
	unsigned int softleft_button_keycode;
	unsigned int softright_button_keycode;
	unsigned int star_button_keycode;
	unsigned int pound_button_keycode;
	sd_bus *bus;
	struct wl_event_source *event;
	battery_callback_t battery_callback;
	void *battery_callback_data;
	bSettings *settings;
};

static int handle_battery_charge(sd_bus_message *msg, void *data,
		sd_bus_error *err)
{
	struct device_info *di = data;
	int32_t chg;
	int res = sd_bus_message_read(msg, "i", &chg);
	if(res < 0){
		fprintf(stderr, "Failed to parse battery charge message: %s",
			strerror(-res));
	}
	else if(di->battery_callback){
		di->battery_callback(chg, di->battery_callback_data);
	}
	return 0;
}

static int handle_battery_charge_upower(sd_bus_message *msg, void *data,
		sd_bus_error *err)
{
	struct device_info *di = data;
	const char *interface, *prop;
	double chg;
	int res;

	/*
	 * Assume that the path points to DisplayDevice because we didn't register
	 * match rules for any other devices
	 */

	res = sd_bus_message_read_basic(msg, 's', &interface);
	if(res < 0)
		goto error;

	/* ignore other interfaces */
	if(0 != strcmp(interface, "org.freedesktop.UPower.Device"))
		return 0;

	/* key/value pairs */
	res = sd_bus_message_enter_container(msg, 'a', "{sv}");
	if(res < 0)
		goto error;
	while(sd_bus_message_enter_container(msg, 'e', "sv") > 0){
		res = sd_bus_message_read_basic(msg, 's', &prop);
		if(res < 0)
			goto error;
		if(0 == strcmp(prop, "Percentage")){
			res = sd_bus_message_enter_container(msg, 'v', "d");
			if(res < 0)
				goto error;
			sd_bus_message_read_basic(msg, 'd', &chg);
			/* got battery percentage, done */
			di->battery_callback(chg, di->battery_callback_data);
			return 0;
		}
		else {
			/* skip other properties */
			sd_bus_message_skip(msg, "v");
		}
		sd_bus_message_exit_container(msg);
	}
error:
	fprintf(stderr, "Failed to parse battery properties from UPower\n");
	return 0;
}

/* Wayland sd_bus integration - taken from the logind backend code */
static int handle_dbus_event(int fd, uint32_t mask, void *data)
{
	sd_bus *bus = data;
	while(sd_bus_process(bus, NULL) > 0) /* do nothing */;
	return 1;
}

struct device_info *device_info_load(struct wl_display *disp, bSettings *settings)
{
	struct wl_event_loop *event_loop;
	struct device_info *di;
	di = calloc(1, sizeof(struct device_info));
	if(!di) return NULL;
	di->settings = settings;
	di->power_button_name = bGetSettingString(settings,
			"input.keys.power.name", "pm8941_pwrkey");
	di->lid_device_name = bGetSettingString(settings,
			"input.keys.lid.name", "gpio_keys");
	di->lid2_device_name = bGetSettingString(settings,
			"input.keys.lid2.name", "gpio_keys");
	di->enter_button_keycode = bGetSettingInt(settings,
			"input.keys.enter.keycode", BTN_MIDDLE);
	di->power_button_keycode = bGetSettingInt(settings,
			"input.keys.power.keycode", KEY_POWER);
	di->back_button_keycode = bGetSettingInt(settings,
			"input.keys.back.keycode", BTN_EXTRA);
	di->endcall_button_keycode = bGetSettingInt(settings,
			"input.keys.endcall.keycode", KEY_HANGUP_PHONE);
	di->call_button_keycode = bGetSettingInt(settings,
			"input.keys.call.keycode", KEY_PICKUP_PHONE);
	di->shortcut_button_keycode = bGetSettingInt(settings,
			"input.keys.shortcut.keycode", KEY_MENU);
	di->assistant_button_keycode = bGetSettingInt(settings,
			"input.keys.assistant.keycode", KEY_SOS);
	di->softleft_button_keycode = bGetSettingInt(settings,
			"input.keys.softleft.keycode", BTN_LEFT);
	di->softright_button_keycode = bGetSettingInt(settings,
			"input.keys.softright.keycode", BTN_RIGHT);
	di->star_button_keycode = bGetSettingInt(settings,
			"input.keys.star.keycode",
			KEY_NUMERIC_STAR);
	di->pound_button_keycode = bGetSettingInt(settings,
			"input.keys.pound.keycode",
			KEY_NUMERIC_POUND);
	sd_bus_default_system(&di->bus);
	if(0 > sd_bus_match_signal(di->bus, NULL, "bananian.PowerManager",
			"/", "bananian.PowerManager", "UpdateCharge",
			handle_battery_charge, di))
	{
		fprintf(stderr, "Failed to add match rule for UpdateCharge\n");
	}
	else {
		event_loop = wl_display_get_event_loop(disp);
		di->event = wl_event_loop_add_fd(event_loop,
			sd_bus_get_fd(di->bus), WL_EVENT_READABLE,
			handle_dbus_event, di->bus);
	}
	if(0 > sd_bus_match_signal(di->bus, NULL, "org.freedesktop.UPower",
			"/org/freedesktop/UPower/devices/DisplayDevice",
			"org.freedesktop.DBus.Properties", "PropertiesChanged",
			handle_battery_charge_upower, di))
	{
		fprintf(stderr, "Failed to add match rule for UPower\n");
	}
	else {
		event_loop = wl_display_get_event_loop(disp);
		di->event = wl_event_loop_add_fd(event_loop,
			sd_bus_get_fd(di->bus), WL_EVENT_READABLE,
			handle_dbus_event, di->bus);
	}
	return di;
}

void device_set_battery_handler(struct device_info *di,
		battery_callback_t cb, void *data)
{
	int ret;
	sd_bus_message *msg = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;

	di->battery_callback = cb;
	di->battery_callback_data = data;

	ret = sd_bus_call_method(di->bus, "bananian.PowerManager",
		"/", "bananian.PowerManager", "GetCharge",
		&error, &msg, "");
	if(ret < 0){
		/* calling bpowerd failed - try upower instead */
		double chg;
		sd_bus_error_free(&error);
		ret = sd_bus_get_property_trivial(di->bus, "org.freedesktop.UPower",
				"/org/freedesktop/UPower/devices/DisplayDevice",
				"org.freedesktop.UPower.Device",
				"Percentage", &error, 'd', &chg);
		if(ret < 0){
			fprintf(stderr, "device: Getting battery charge failed: %s\n",
				error.message);
			cb(100, data);
		}
		else {
			cb(chg, data);
		}
	}
	else {
		/* calling bpowerd successful */
		int chg;
		ret = sd_bus_message_read(msg, "i", &chg);
		if(ret < 0){
			fprintf(stderr, "Failed to parse battery charge message: %s",
				strerror(-ret));
			cb(100, data);
		}
		else {
			cb(chg, data);
		}
	}

	sd_bus_error_free(&error);
	sd_bus_message_unref(msg);
}

int device_should_wakeup(struct device_info *di, const char *input_name,
		uint32_t keycode, int released)
{
	if(released){
		if(di->lid_device_name && input_name &&
			0 == strcmp(input_name, di->lid_device_name) &&
			keycode == DEV_KEY_SWITCH)
		{
			return 1;
		}
	}
	else {
		//if(keycode == KEY_RIGHTALT) return 1;
		if(di->power_button_name && input_name &&
			0 == strcmp(input_name, di->power_button_name))
		{
			return 1;
		}
	}
	return 0;
}
int device_should_sleep(struct device_info *di, const char *input_name,
		uint32_t keycode, int released)
{
	if(released){
		//if(keycode == KEY_RIGHTALT) return 1;
		if(di->power_button_name && input_name
			&& keycode == di->power_button_keycode
			&& 0 == strcmp(input_name, di->power_button_name))
		{
			return 1;
		}
	}
	else {
		if(di->lid_device_name && input_name &&
			0 == strcmp(input_name, di->lid_device_name) &&
			keycode == DEV_KEY_SWITCH)
		{
			return 1;
		}
	}
	return 0;
}

uint32_t device_switch_keycode(struct device_info *di, const char *input_name,
		int on)
{
	if(di->lid_device_name && input_name &&
		0 == strcmp(input_name, di->lid_device_name))
	{
		if(on)
			return KEY_SUSPEND;
		else
			return KEY_DISPLAYTOGGLE;
	}
	else if(di->lid2_device_name && input_name &&
		0 == strcmp(input_name, di->lid2_device_name))
	{
		if(on)
			return KEY_RIGHTCTRL;
		else
			return KEY_DISPLAYTOGGLE;
	}
	return 0;
}

uint32_t device_patch_keycode(struct device_info *di, const char *input_name,
		uint32_t orig)
{
	if(!di) return orig;
	if(orig == di->enter_button_keycode)
		return KEY_ENTER;
	else if(orig == di->star_button_keycode) /* '*' */
		return KEY_LEFTCTRL;
	else if(orig == di->pound_button_keycode) /* '#' */
		return KEY_LEFTALT;
	else if(orig == di->softleft_button_keycode)
		return KEY_TAB;
	else if(orig == di->softright_button_keycode)
		return KEY_COMPOSE;
	/*
	 * Special case because endcall or back button
	 * sends KEY_POWER on some devices
	 */
	else if(orig == di->power_button_keycode &&
		di->power_button_name && input_name &&
		0 == strcmp(input_name, di->power_button_name))
	{
		return KEY_POWER;
	}
	else if(orig == di->back_button_keycode)
		return KEY_BACKSPACE;
	else if(orig == di->endcall_button_keycode)
		return KEY_ESC;
	else if(orig == di->call_button_keycode)
		return KEY_F3;
	else if(orig == di->assistant_button_keycode)
		return KEY_F1;
	else if(orig == di->shortcut_button_keycode)
		return KEY_PRINT;
	return orig;
}

void device_suspend(struct device_info *di)
{
	int ret;
	sd_bus_message *msg = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;

	/*
	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1", "org.freedesktop.login1.Manager",
		"Suspend", &error, &msg, "b", 0);
	if(ret < 0){
		fprintf(stderr, "device: Suspend via systemd failed: %s\n",
			error.message);
	}
	*/

	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1/session/auto",
		"org.freedesktop.login1.Session", "SetIdleHint",
		&error, &msg, "b", 1);
	if(ret < 0){
		fprintf(stderr, "device: Setting session idle failed: %s\n",
			error.message);
	}

	sd_bus_error_free(&error);
	sd_bus_message_unref(msg);
}

void device_resume(struct device_info *di)
{
	int ret;
	sd_bus_message *msg = NULL;
	sd_bus_error error = SD_BUS_ERROR_NULL;

	ret = sd_bus_call_method(di->bus, "bananian.WakeupManager",
		"/", "bananian.WakeupManager", "WakeUp",
		&error, &msg, "b", 0);
	if(ret < 0){
		fprintf(stderr, "device: WakeupManager wakeup failed: %s\n",
			error.message);
	}

	ret = sd_bus_call_method(di->bus, "org.freedesktop.login1",
		"/org/freedesktop/login1/session/auto",
		"org.freedesktop.login1.Session", "SetIdleHint",
		&error, &msg, "b", 0);
	if(ret < 0){
		fprintf(stderr, "device: Setting session idle failed: %s\n",
			error.message);
	}

	sd_bus_error_free(&error);
	sd_bus_message_unref(msg);
}

void device_info_free(struct device_info *di)
{
	if(!di) return;
	free(di->power_button_name);
	free(di->lid_device_name);
	free(di->lid2_device_name);
	free(di);
}
