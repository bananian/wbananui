/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output_damage.h>
#include <wlr/types/wlr_output.h>
#include "server.h"
#include "navigation.h"
#include "topbar.h"
#include "homescreen.h"
#include "output.h"
#include "render.h"

#define timediff(_a, _b) \
	(((_b).tv_nsec - (_a).tv_nsec) / 1000 + \
	 1000000 * ((_b).tv_sec - (_a).tv_sec))

struct output_item {
	struct wlr_output *output;
	struct wlr_output_damage *damage;
	struct server_data *server;
	struct timespec last_frame;

	struct wl_listener destroy;
	struct wl_listener frame;

	struct wl_list link;
};

static void output_destroy_notify(struct wl_listener *listener, void *data)
{
	struct output_item *out = wl_container_of(listener, out, destroy);
	wlr_output_damage_destroy(out->damage);
	wl_list_remove(&out->link);
	wl_list_remove(&out->destroy.link);
	wl_list_remove(&out->frame.link);
}

static void output_damage_frame_notify(struct wl_listener *listener, void *data)
{
	struct output_item *out = wl_container_of(listener, out, frame);
	struct server_data *srv = out->server;
	struct wlr_output *wlr_out = out->output;
	struct wlr_renderer *renderer;
	struct timespec now;
	bool needs_frame;
	pixman_region32_t dmg;

	clock_gettime(CLOCK_MONOTONIC, &now);

	render_damage(srv);

	renderer = wlr_backend_get_renderer(wlr_out->backend);

	pixman_region32_init(&dmg);
	if(!wlr_output_damage_attach_render(out->damage, &needs_frame, &dmg)){
		return;
	}

	if(!needs_frame){
		wlr_output_rollback(wlr_out);
		goto frame_end;
	}

	wlr_renderer_begin(renderer, srv->fullscreen_width,
			srv->fullscreen_height);

	render_apps(srv, renderer, now, wlr_out,
		timediff(out->last_frame, now));

	wlr_renderer_end(renderer);

	wlr_output_set_damage(wlr_out, &dmg);
	if(!wlr_output_commit(wlr_out)){
		fprintf(stderr, "Failed to commit buffer to output!\n");
	}
	out->last_frame = now;
	
frame_end:
	pixman_region32_fini(&dmg);
}


void new_output_notify(struct wl_listener *listener, void *data)
{
	struct server_data *srv = wl_container_of(listener, srv, new_output);
	struct wlr_output *output = data;
	struct output_item *oi;
	struct wlr_renderer *renderer;

	oi = calloc(1, sizeof(struct output_item));
	clock_gettime(CLOCK_MONOTONIC, &oi->last_frame);
	oi->server = srv;
	oi->output = output;
	wl_list_insert(&srv->outputs, &oi->link);

	oi->destroy.notify = output_destroy_notify;
	wl_signal_add(&output->events.destroy, &oi->destroy);

	oi->damage = wlr_output_damage_create(output);

	oi->frame.notify = output_damage_frame_notify;
	wl_signal_add(&oi->damage->events.frame, &oi->frame);

	wlr_output_enable(output, 1);
	if(!wl_list_empty(&output->modes)){
		struct wlr_output_mode *mode, *last = NULL;
		fprintf(stderr, "[DEBUG] Available modes:\n");
		wl_list_for_each(mode, &output->modes, link){
			fprintf(stderr, "[DEBUG]  - %dx%d@%dMHz\n",
				mode->width, mode->height, mode->refresh);
			last = mode;
		}
		if(last){
			fprintf(stderr, "[DEBUG] Setting output mode\n");
			wlr_output_set_mode(output, last);
		}
	}
	wlr_output_set_scale(output, 1);

	if(!wlr_output_commit(output)){
		fprintf(stderr, "Failed to commit mode for %s!\n",
			output->name);
	}

	wlr_output_create_global(output);

	srv->fullscreen_width = output->width;
	srv->fullscreen_height = output->height;
	srv->viewport_width = srv->fullscreen_width;
	srv->viewport_height = srv->fullscreen_height - TOPBAR_HEIGHT;

	renderer = wlr_backend_get_renderer(srv->backend);

	if(!srv->topbar){
		srv->topbar = init_topbar(output->width, TOPBAR_HEIGHT,
				renderer);
	}
	if(!srv->homescreen){
		srv->homescreen = init_homescreen(output->width, output->height,
				renderer);
	}
	if(!srv->nav){
		srv->nav = init_navigation(output->width, output->height,
				renderer);
	}

}

void turn_off_outputs(struct server_data *srv)
{
	struct output_item *out;
	wl_list_for_each(out, &srv->outputs, link){
		wlr_output_enable(out->output, false);
		wlr_output_commit(out->output);
	}
}
void turn_on_outputs(struct server_data *srv)
{
	struct output_item *out;
	wl_list_for_each(out, &srv->outputs, link){
		wlr_output_enable(out->output, true);
		wlr_output_commit(out->output);
	}
}

void damage_all_outputs(struct server_data *srv)
{
	struct output_item *out;
	wl_list_for_each(out, &srv->outputs, link){
		wlr_output_damage_add_whole(out->damage);
	}
}

void damage_app_output(struct server_data *srv)
{
	struct output_item *out;
	/* For now, the first output is used as the "app" output
	 * (the one which shows the apps).
	 */
	wl_list_for_each(out, &srv->outputs, link){
		wlr_output_damage_add_whole(out->damage);
		break;
	}
}

void damage_app_output_region(struct server_data *srv, pixman_region32_t *dmg)
{
	struct output_item *out;
	/* For now, the first output is used as the "app" output
	 * (the one which shows the apps).
	 */
	wl_list_for_each(out, &srv->outputs, link){
		wlr_output_damage_add(out->damage, dmg);
		break;
	}
}
