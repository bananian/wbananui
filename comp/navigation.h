/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_NAVIGATION_H_
#define _COMP_NAVIGATION_H_

#include <xkbcommon/xkbcommon.h>

#define WINLIST_WINDOW_SCALE 0.8
#define WINLIST_TITLE_HEIGHT 30

struct wlr_renderer;
struct wlr_texture;

struct server_data;

struct navigation_data {
	double wl_pos, wl_xoffset, wl_dxoffset,
		wl_destxoffset;
	int wl_dpos, wl_selected, wl_max;
	struct wlr_texture *no_window_texture, *nav_texture;
	bool nav_open, wl_openclose, wl_move, damage;
};

void winlist_close(struct server_data *srv, bool animate);
void nav_handle_key(struct server_data *srv, xkb_keysym_t sym,
		uint32_t modifiers, int released);
void winlist_handle_key(struct server_data *srv, xkb_keysym_t sym,
		uint32_t modifiers);
struct wlr_texture *nav_create_title_texture(struct server_data *srv,
		const char *title);
struct navigation_data *init_navigation(unsigned int width, unsigned int height,
		struct wlr_renderer *renderer);
void navigation_damage(struct server_data *srv);
void navigation_surfaces_damaged(struct server_data *srv);
void render_navigation(struct server_data *srv, struct wlr_renderer *renderer,
		struct timespec now, struct wlr_output *out, long time);

#endif
