/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_WM_H_
#define _COMP_WM_H_

#include <wlr/types/wlr_surface.h>

struct server_data;
struct toplevel_item;

struct window_manager {
	struct toplevel_item *active, *underlay;
	unsigned short int hs_launching_countdown;
};

void toplevel_activate(struct server_data *srv, struct toplevel_item *tl);
void new_xdg_surface_notify(struct wl_listener *listener, void *data);
void new_surface_notify(struct wl_listener *listener, void *data);
struct window_manager *init_wm(void);

bool window_is_topbar_hidden(struct toplevel_item *ti);
void window_render(struct toplevel_item *ti, struct wlr_renderer *renderer,
		struct timespec now, struct wlr_output *out);
int window_render_side_by_side(struct server_data *srv,
		struct wlr_renderer *renderer, struct timespec now,
		struct wlr_output *out, int spacing,
		int xoffset, int yoffset);
int window_get_index(struct toplevel_item *ti);
struct toplevel_item *window_find_by_index(struct server_data *srv, int index);
void window_close(struct toplevel_item *ti);

#endif
