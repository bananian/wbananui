/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_DEVICEINFO_H_
#define _COMP_DEVICEINFO_H_

#include <wayland-server.h>
#include <stdint.h>

#define DEV_KEY_SWITCH 249

struct device_info;

typedef void (*battery_callback_t)(int charge, void *data);

struct device_info *device_info_load(struct wl_display *disp,
		bSettings *settings);
void device_info_free(struct device_info *di);
void device_set_battery_handler(struct device_info *di,
		battery_callback_t cb, void *data);
void device_suspend(struct device_info *di);
void device_resume(struct device_info *di);
void device_ensure_awake(struct device_info *di);
uint32_t device_switch_keycode(struct device_info *di, const char *input_name,
		int on);
uint32_t device_patch_keycode(struct device_info *di, const char *input_name,
		uint32_t orig);
int device_should_wakeup(struct device_info *di, const char *input_name,
		uint32_t keycode, int released);
int device_should_sleep(struct device_info *di, const char *input_name,
		uint32_t keycode, int released);

#endif
