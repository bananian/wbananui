/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_keyboard.h>
#include <linux/input-event-codes.h>
#include <bananui/keys.h>
#include "server.h"
#include "output.h"
#include "deviceinfo.h"
#include "navigation.h"
#include "homescreen.h"
#include "textinput.h"
#include "wm.h"
#include "input.h"

#define UISTATELOG 1

struct input_data {
	int powerkey;
	bool star_pressed, pound_pressed;
};

struct input_item {
	struct wlr_input_device *input;
	struct server_data *server;

	uint32_t last_scroll;
	double delta;

	struct wl_listener destroy;
	struct wl_listener key;
	struct wl_listener scroll;

	struct wl_list link;
};

static bool keyboard_handle_xkb(struct server_data *srv,
	xkb_keysym_t sym, uint32_t modifiers, enum wlr_key_state state)
{
	if(state == WLR_KEY_PRESSED){
		if(srv->sleeping) return 1;
		if(sym == BANANUI_KEY_Pound)
			srv->inp->pound_pressed = true;
		if(sym == BANANUI_KEY_Star)
			srv->inp->star_pressed = true;

		if(srv->nav->nav_open)
			nav_handle_key(srv, sym, modifiers, 0);
		else if(srv->nav->wl_pos > 0)
			winlist_handle_key(srv, sym, modifiers);
		else if(((srv->inp->star_pressed && sym == BANANUI_KEY_Pound) ||
			(srv->inp->pound_pressed && sym == BANANUI_KEY_Star)) ||
			sym == BANANUI_KEY_Shortcut)
		{
			srv->nav->nav_open = true;
			damage_app_output(srv);
		}
		else if(srv->wm->active == NULL){
			if(srv->wm->hs_launching_countdown){
				if(srv->wm->hs_launching_countdown < 15 &&
					(sym == XKB_KEY_BackSpace ||
					sym == BANANUI_KEY_EndCall))
				{
					/*
					 * Let impatient users go back to
					 * the home screen. The app might
					 * still pop up after this
					 */
					srv->wm->hs_launching_countdown = 0;
					damage_app_output(srv);
				}
				return true;
			}

			switch(homescreen_handle_key(srv->homescreen, sym,
					false))
			{
				case HS_LAUNCHING:
					srv->wm->hs_launching_countdown = 20;
				case HS_RENDER:
					damage_app_output(srv);
					break;
				case HS_IGNORE:
					break;
			}
		}
		else if(!text_input_handle_keypress(
			srv->txtinp, sym))
		{
			return false;
		}
	}
	else {
		if(sym == BANANUI_KEY_Pound)
			srv->inp->pound_pressed = false;
		if(sym == BANANUI_KEY_Star)
			srv->inp->star_pressed = false;
		if(srv->sleeping) return true;

		if(srv->nav->nav_open)
			nav_handle_key(srv, sym, modifiers, 1);
		else if(srv->nav->wl_pos > 0)
			/* empty */;
		else if(srv->wm->active == NULL){
			if(srv->wm->hs_launching_countdown) return true;
			switch(homescreen_handle_key(srv->homescreen, sym,
					true))
			{
				case HS_LAUNCHING:
					srv->wm->hs_launching_countdown = 20;
				case HS_RENDER:
					damage_app_output(srv);
					break;
				case HS_IGNORE:
					break;
			}
		}
		else if(!text_input_handle_keyrelease(
			srv->txtinp, sym))
		{
			return false;
		}

	}
	return true;
}

static void switch_toggle_notify(struct wl_listener *listener, void *data)
{
	struct input_item *ii = wl_container_of(listener, ii, key);
	struct server_data *srv = ii->server;
	struct wlr_event_switch_toggle *ev = data;
	uint32_t keycode = device_switch_keycode(srv->di, ii->input->name,
			ev->switch_state == WLR_SWITCH_STATE_ON);
	wlr_seat_keyboard_notify_key(srv->seat, 0, keycode, WLR_KEY_PRESSED);
	wlr_seat_keyboard_notify_key(srv->seat, 0, keycode, WLR_KEY_RELEASED);
	if(srv->sleeping && ev->switch_state == WLR_SWITCH_STATE_ON &&
		device_should_wakeup(srv->di, ii->input->name,
			DEV_KEY_SWITCH, 0))
	{
#if UISTATELOG
		fprintf(stderr, "[ui_state] SLEEP -> (switch on) -> WAKE\n");
#endif
		ui_wakeup(srv);
	}
	else if(srv->sleeping && ev->switch_state == WLR_SWITCH_STATE_OFF &&
		device_should_wakeup(srv->di, ii->input->name,
			DEV_KEY_SWITCH, 1))
	{
#if UISTATELOG
		fprintf(stderr, "[ui_state] SLEEP -> (switch off) -> WAKE\n");
#endif
		ui_wakeup(srv);
	}
	else if(!srv->sleeping &&
		device_should_sleep(srv->di, ii->input->name, DEV_KEY_SWITCH,
			ev->switch_state == WLR_SWITCH_STATE_OFF))
	{
#if UISTATELOG
		fprintf(stderr, "[ui_state] WAKE -> (switch) -> SLEEP\n");
#endif
		ui_sleep(srv);
	}
}

static void generic_key(struct server_data *srv, struct input_item *kbi,
		const char *input_name,
		enum wlr_key_state state,
		uint32_t orig_keycode)
{
	bool handled = false;
	int i;
	const xkb_keysym_t *keysyms;
	size_t numsyms;
	uint32_t modifiers;
	xkb_keycode_t keycode =
		device_patch_keycode(srv->di, input_name, orig_keycode)
		+ 8;

	if(!kbi){
		wl_list_for_each(kbi, &srv->inputs, link){
			if(kbi->input->type == WLR_INPUT_DEVICE_KEYBOARD)
				break;
		}
	}
	if(kbi->input->type != WLR_INPUT_DEVICE_KEYBOARD)
		return;

	numsyms = xkb_keymap_key_get_syms_by_level(
			kbi->input->keyboard->keymap,
			keycode,
			xkb_state_key_get_layout(
				kbi->input->keyboard->xkb_state,
				keycode),
			0,
			&keysyms);
	modifiers = wlr_keyboard_get_modifiers(kbi->input->keyboard);
	for(i = 0; i < numsyms; i++){
		handled = handled || keyboard_handle_xkb(srv, keysyms[i],
				modifiers, state);
	}

	if(!handled){
		wlr_seat_keyboard_notify_modifiers(srv->seat,
			&kbi->input->keyboard->modifiers);
		wlr_seat_keyboard_notify_key(srv->seat, 0, keycode - 8,
			state);
	}
}

static void keyboard_key_notify(struct wl_listener *listener, void *data)
{
	struct input_item *ii = wl_container_of(listener, ii, key);
	struct server_data *srv = ii->server;
	struct wlr_event_keyboard_key *ev = data;

	if(srv->sleeping &&
		device_should_wakeup(srv->di, ii->input->name, ev->keycode,
			ev->state == WLR_KEY_RELEASED))
	{
#if UISTATELOG
		fprintf(stderr,
			"[ui_state] SLEEP -> (key) -> WAKE\n");
#endif
		ui_wakeup(srv);
		return;
	}
	if(!srv->sleeping &&
		ev->state == WLR_KEY_PRESSED &&
		device_should_sleep(srv->di, ii->input->name, ev->keycode, 0))
	{
#if UISTATELOG
		fprintf(stderr, "[ui_state] WAKE -> (keydown) -> SLEEP\n");
#endif
		ui_sleep(srv);
		return;
	}
	if(srv->inp->powerkey &&
		ev->state == WLR_KEY_RELEASED &&
		device_should_sleep(srv->di, ii->input->name, ev->keycode, 1))
	{
		srv->inp->powerkey--;
		if(srv->inp->powerkey){
#if UISTATELOG
			fprintf(stderr,
				"[ui_state] WAKE -> (keyup) -> WAIT\n");
#endif
		}
		else if(!srv->sleeping){
#if UISTATELOG
			fprintf(stderr,
				"[ui_state] WAKE -> (keyup) -> SLEEP\n");
#endif
			ui_sleep(srv);
		}
		return;
	}

	generic_key(srv, ii, ii->input->name, ev->state, ev->keycode);

	if(ev->state == WLR_KEY_PRESSED &&
		device_should_sleep(srv->di, ii->input->name, ev->keycode, 1))
	{
		srv->inp->powerkey++;
#if UISTATELOG
		fprintf(stderr, "[ui_state] powerkey pressed\n");
#endif
	}
}

static void scroll_notify(struct wl_listener *listener, void *data)
{
	struct input_item *ii = wl_container_of(listener, ii, scroll);
	struct server_data *srv = ii->server;
	struct wlr_event_pointer_axis *ev = data;
	if((ev->time_msec - ii->last_scroll) >= 1000){
		ii->delta = 0;
	}
	ii->delta += ev->delta;
	if(ii->delta < 10 && ii->delta > -10){
		ii->last_scroll = ev->time_msec;
		return;
	}
	generic_key(srv, NULL, ii->input->name, WLR_KEY_PRESSED,
			ev->orientation == WLR_AXIS_ORIENTATION_VERTICAL
			? (ii->delta < 0 ? KEY_UP : KEY_DOWN)
			: (ii->delta < 0 ? KEY_LEFT : KEY_RIGHT));
	generic_key(srv, NULL, ii->input->name, WLR_KEY_RELEASED,
			ev->orientation == WLR_AXIS_ORIENTATION_VERTICAL
			? (ii->delta < 0 ? KEY_UP : KEY_DOWN)
			: (ii->delta < 0 ? KEY_LEFT : KEY_RIGHT));
	ii->last_scroll = 0;
}

static void button_notify(struct wl_listener *listener, void *data)
{
	struct input_item *ii = wl_container_of(listener, ii, key);
	struct server_data *srv = ii->server;
	struct wlr_event_pointer_button *ev = data;
	generic_key(srv, NULL, ii->input->name, ev->state == WLR_BUTTON_RELEASED
			? WLR_KEY_RELEASED : WLR_KEY_PRESSED,
			ev->button);
}

static void input_destroy_notify(struct wl_listener *listener, void *data)
{
	struct input_item *ii = wl_container_of(listener, ii, destroy);
	switch(ii->input->type){
		case WLR_INPUT_DEVICE_KEYBOARD:
			wl_list_remove(&ii->scroll.link);
		case WLR_INPUT_DEVICE_POINTER:
		case WLR_INPUT_DEVICE_SWITCH:
			wl_list_remove(&ii->key.link);
			break;
		default:
			break;
	}
	wl_list_remove(&ii->destroy.link);
	wl_list_remove(&ii->link);
}

void new_input_notify(struct wl_listener *listener, void *data)
{
	struct server_data *srv = wl_container_of(listener, srv, new_input);
	struct wlr_input_device *input = data;
	struct input_item *ii;
	struct xkb_rule_names xkbrules = {0};
	struct xkb_context *xkbcontext;
	struct xkb_keymap *xkbkeymap;

	ii = calloc(1, sizeof(struct input_item));
	ii->server = srv;
	ii->input = input;
	wl_list_insert(&srv->inputs, &ii->link);
	ii->destroy.notify = input_destroy_notify;
	wl_signal_add(&input->events.destroy, &ii->destroy);

	switch(input->type){
		case WLR_INPUT_DEVICE_SWITCH:
			ii->key.notify = switch_toggle_notify;
			wl_signal_add(&input->switch_device->events.toggle,
				&ii->key);
			break;
		case WLR_INPUT_DEVICE_POINTER:
			ii->key.notify = button_notify;
			ii->scroll.notify = scroll_notify;
			wl_signal_add(&input->pointer->events.button,
					&ii->key);
			wl_signal_add(&input->pointer->events.axis,
					&ii->scroll);
			break;
		case WLR_INPUT_DEVICE_KEYBOARD:
			ii->key.notify = keyboard_key_notify;
			wl_signal_add(&input->keyboard->events.key, &ii->key);
			xkbrules.rules = getenv("XKB_DEFAULT_RULES");
			xkbrules.model = getenv("XKB_DEFAULT_MODEL");
			xkbrules.layout = getenv("XKB_DEFAULT_LAYOUT");
			xkbrules.variant = getenv("XKB_DEFAULT_VARIANT");
			xkbrules.options = getenv("XKB_DEFAULT_OPTIONS");
			xkbcontext = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
			if (!xkbcontext) {
				fprintf(stderr, "Failed to create XKB context");
				break;
			}
			xkbkeymap = xkb_map_new_from_names(xkbcontext,
				&xkbrules, XKB_KEYMAP_COMPILE_NO_FLAGS);
			if (!xkbkeymap) {
				fprintf(stderr, "Failed to create XKB keymap");
				exit(1);
			}
			wlr_keyboard_set_keymap(input->keyboard, xkbkeymap);
			xkb_keymap_unref(xkbkeymap);
			xkb_context_unref(xkbcontext);
			wlr_seat_set_keyboard(srv->seat, input);
			wlr_seat_set_capabilities(srv->seat, 0x2);
			break;
		default:
			break;
	}
	fprintf(stderr, "[DEBUG] Input added\n");
}

struct input_data *init_input(void)
{
	struct input_data *inp;

	inp = calloc(1, sizeof(struct input_data));
	if(!inp) return NULL;

	inp->powerkey = 0;
	inp->pound_pressed = inp->star_pressed = false;

	return inp;
}
