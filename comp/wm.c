/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <bananui/widget.h>
#include <bananui/window_title.h>
#include "server.h"
#include "topbar.h"
#include "output.h"
#include "textinput.h"
#include "navigation.h"
#include "wm.h"

struct surface_item {
	struct wlr_surface *surface;
	struct server_data *server;

	struct wl_listener destroy;
	struct wl_listener commit;

	struct wl_list link;
};

enum window_shell_type {
	SHELL_XDG
};

struct toplevel_item {
	enum window_shell_type shell;
	union {
		struct wlr_xdg_surface *xdg_surface;
	};
	struct wl_client *client;
	struct server_data *server;

	unsigned char *title_pixels;
	struct wlr_texture *title_texture; /* Shown in the window list */

	struct wl_listener commit;
	struct wl_listener destroy;
	struct wl_listener map;
	struct wl_listener request_fullscreen;
	struct wl_listener set_title;

	bool fullscreen, is_alert_screen, is_topbar_overlapped;

	struct wl_list link;
};

struct surface_render_data {
	struct timespec now;
	struct wlr_output *out;
	struct wlr_renderer *renderer;
	bool fullscreen;
	int xpos, ypos;
	float scale;
};

static void xdg_toplevel_activate(struct server_data *srv,
	struct wlr_xdg_surface *xs)
{
	wlr_seat_keyboard_notify_enter(srv->seat, xs->surface, NULL, 0, NULL);
	wlr_xdg_toplevel_set_activated(xs, 1);
	text_input_focus(srv->txtinp, xs->surface);
}
static void xdg_toplevel_deactivate(struct server_data *srv,
	struct wlr_xdg_surface *xs)
{
	wlr_xdg_toplevel_set_activated(xs, 0);
	text_input_unfocus(srv->txtinp, xs->surface);
}

static void toplevel_deactivate(struct server_data *srv,
	struct toplevel_item *tl)
{
	if(tl){
		tl->fullscreen = false;
		switch(tl->shell){
			case SHELL_XDG:
				wlr_xdg_toplevel_set_fullscreen(
					tl->xdg_surface, 0);
				wlr_xdg_toplevel_set_size(
					tl->xdg_surface,
					srv->viewport_width,
					srv->viewport_height);
				wlr_xdg_toplevel_set_maximized(
					tl->xdg_surface, 1);
				xdg_toplevel_deactivate(srv,
					tl->xdg_surface);
				break;
		}
	}
}
void toplevel_activate(struct server_data *srv, struct toplevel_item *tl)
{
	if(tl && tl->is_alert_screen){
		if(!srv->wm->underlay) srv->wm->underlay = srv->wm->active;
	}
	else {
		srv->wm->underlay = NULL;
	}
	toplevel_deactivate(srv, srv->wm->active);
	srv->wm->active = tl;
	srv->wm->hs_launching_countdown = 0;
	if(!tl || tl->is_topbar_overlapped){
		topbar_set_color(srv->topbar, 0.0, 0.0, 0.0, 0.0);
	}
	else {
		topbar_set_color_default(srv->topbar);
	}
	if(tl){
		switch(tl->shell){
			case SHELL_XDG:
				xdg_toplevel_activate(srv,
					tl->xdg_surface);
				break;
		}
	}
	damage_app_output(srv);
}

static struct wlr_texture *create_title_texture(struct server_data *srv,
		struct toplevel_item *ti)
{
	unsigned int height = WINLIST_TITLE_HEIGHT, width, stride;
	const char *title = ti->xdg_surface->toplevel->title ?
			ti->xdg_surface->toplevel->title :
			ti->xdg_surface->toplevel->app_id ?
			ti->xdg_surface->toplevel->app_id : "";
	cairo_surface_t *csurf;
	cairo_t *cr;
	bBoxWidget *mainbox;
	bContentWidget *text;

	if(ti->title_pixels){
		free(ti->title_pixels);
		ti->title_pixels = NULL;
	}
	if(ti->title_texture){
		wlr_texture_destroy(ti->title_texture);
		ti->title_texture = NULL;
	}
	width = srv->fullscreen_width * WINLIST_WINDOW_SCALE;
	stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, width);
	ti->title_pixels = calloc(1, stride*height);
	if(!ti->title_pixels) return NULL;
	csurf = cairo_image_surface_create_for_data(ti->title_pixels,
		CAIRO_FORMAT_ARGB32, width, height, stride);
	if(!csurf) return NULL;
	cr = cairo_create(csurf);
	if(!cr){
		cairo_surface_destroy(csurf);
		return NULL;
	}

	mainbox = bCreateBoxWidget(B_ALIGN_CENTER, B_BOX_HBOX,
				width, height);
	text = bCreateContentWidget(B_ALIGN_CENTER, 0);
	bSetTextContent(text, title, PANGO_WEIGHT_SEMIBOLD, 17);

	text->color = bRGBA(1.0, 1.0, 1.0, 1.0);
	bAddContentToBox(mainbox, text);

	bShowMainBox(csurf, cr, mainbox, 0, 0);

	bDestroyBoxRecursive(mainbox);

	cairo_destroy(cr);
	cairo_surface_destroy(csurf);
	return wlr_texture_from_pixels(
		wlr_backend_get_renderer(srv->backend),
		WL_SHM_FORMAT_ARGB8888, stride, width, height,
		ti->title_pixels);
}

static void process_title(struct toplevel_item *ti)
{
	const char *title = ti->xdg_surface->toplevel->title;
	if(!title) return;
	ti->is_alert_screen = !strncmp(title, TITLE_PREFIX_ALERT_SCREEN,
		TITLE_PREFIX_ALERT_SCREEN_LEN);
	ti->is_topbar_overlapped = !strncmp(title, TITLE_PREFIX_TOPBAR_OVERLAP,
		TITLE_PREFIX_TOPBAR_OVERLAP_LEN);
}

static struct wlr_surface *locate_surface(struct wlr_surface *surf,
	int *sx, int *sy)
{
	if(wlr_surface_is_xdg_surface(surf)){
		return surf;
	}
	if(wlr_surface_is_subsurface(surf)){
		struct wlr_subsurface *subsurf;
		subsurf = wlr_subsurface_from_wlr_surface(surf);
		*sx += subsurf->current.x;
		*sy += subsurf->current.y;
		return locate_surface(subsurf->parent, sx, sy);
	}
	return NULL;
}

static void surface_commit_notify(struct wl_listener *listener, void *data)
{
	struct surface_item *si = wl_container_of(listener, si, commit);
	struct server_data *srv = si->server;
	struct wlr_surface *root;
	struct wlr_xdg_surface *xdgsurf;
	int sx = 0, sy = 0;
	pixman_region32_t damage;

	root = locate_surface(si->surface, &sx, &sy);
	if(!root)
		return;
	xdgsurf = wlr_xdg_surface_from_wlr_surface(root);

	navigation_surfaces_damaged(srv);

	if((!srv->wm->active || xdgsurf != srv->wm->active->xdg_surface) &&
	(!srv->wm->underlay || xdgsurf != srv->wm->underlay->xdg_surface))
	{
		return;
	}

	pixman_region32_init(&damage);
	wlr_surface_get_effective_damage(si->surface, &damage);
	if(!xdgsurf->toplevel->current.fullscreen){
		pixman_region32_translate(&damage, 0, TOPBAR_HEIGHT);
	}
	pixman_region32_translate(&damage, sx, sy);
	damage_app_output_region(srv, &damage);
	pixman_region32_fini(&damage);
}

static void xdg_toplevel_set_title_notify(struct wl_listener *listener,
	void *data)
{
	struct toplevel_item *ti = wl_container_of(listener, ti, set_title);
	struct server_data *srv = ti->server;
	if(ti->is_alert_screen) return;
	ti->title_texture = create_title_texture(srv, ti);
	process_title(ti);
}

static void xdg_toplevel_request_fullscreen_notify(struct wl_listener *listener,
	void *data)
{
	struct wlr_xdg_toplevel_set_fullscreen_event *ev = data;
	struct toplevel_item *ti =
		wl_container_of(listener, ti, request_fullscreen);
	struct server_data *srv = ti->server;
	if(ev->fullscreen){
		fprintf(stderr, "[DEBUG] XDG surface went fullscreen\n");
	}
	else {
		fprintf(stderr, "[DEBUG] XDG surface exited fullscreen\n");
	}
	ti->fullscreen = ev->fullscreen;
	wlr_xdg_toplevel_set_fullscreen(ev->surface, ev->fullscreen);
	wlr_xdg_toplevel_set_size(ev->surface,
		srv->fullscreen_width, srv->fullscreen_height);
}
static void xdg_surface_destroy_notify(struct wl_listener *listener,
	void *data)
{
	struct toplevel_item *si = wl_container_of(listener, si, destroy);
	struct server_data *srv = si->server;
	struct toplevel_item *tmp;
	if(srv->wm->underlay == si){
		srv->wm->underlay = NULL;
	}
	if(srv->wm->active == si){
		/* toplevel_activate should not deactivate this window
		 * because it is in the process of being destroyed */
		srv->wm->active = NULL;

		if(si->is_alert_screen){
			toplevel_activate(srv, srv->wm->underlay);
		}
		else {
			int activated = 0;
			wl_list_for_each(tmp, &srv->toplevels, link){
				if(tmp == si) continue;
				if(si->client != tmp->client) continue;
				toplevel_activate(srv, tmp);
				activated = 1;
				break;
			}
			if(!activated) toplevel_activate(srv, NULL);
		}
	}
	if(si->title_texture) wlr_texture_destroy(si->title_texture);
	if(si->title_pixels) free(si->title_pixels);
	wl_list_remove(&si->destroy.link);
	wl_list_remove(&si->map.link);
	wl_list_remove(&si->link);
	damage_app_output(srv);
	fprintf(stderr, "[DEBUG] Removed xdg surface\n");
}
static void xdg_surface_map_notify(struct wl_listener *listener, void *data)
{
	struct toplevel_item *ti = wl_container_of(listener, ti, map);
	struct server_data *srv = ti->server;
	if(ti->xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) return;
	ti->title_texture = create_title_texture(srv, ti);
	process_title(ti);
	fprintf(stderr, "[DEBUG] XDG surface mapped\n");
	if(!srv->wm->active || ti->is_alert_screen ||
		srv->wm->active->client == ti->client)
	{
		toplevel_activate(srv, ti);
	}
	if(ti->is_alert_screen && srv->sleeping) ui_wakeup(srv);
}

void new_xdg_surface_notify(struct wl_listener *listener, void *data)
{
	struct server_data *srv = wl_container_of(listener, srv,
		new_xdg_surface);
	struct wlr_xdg_surface *surf = data;
	struct toplevel_item *si_top;
	if(surf->role == WLR_XDG_SURFACE_ROLE_TOPLEVEL){
		si_top = calloc(1, sizeof(struct toplevel_item));
		si_top->server = srv;
		si_top->shell = SHELL_XDG;
		si_top->xdg_surface = surf;
		si_top->client = wl_resource_get_client(surf->resource);
		si_top->title_texture = NULL;
		si_top->destroy.notify = xdg_surface_destroy_notify;
		wl_signal_add(&surf->events.destroy, &si_top->destroy);
		si_top->map.notify = xdg_surface_map_notify;
		wl_signal_add(&surf->events.map, &si_top->map);

		si_top->request_fullscreen.notify =
			xdg_toplevel_request_fullscreen_notify;
		wl_signal_add(&surf->toplevel->events.request_fullscreen,
			&si_top->request_fullscreen);

		si_top->set_title.notify =
			xdg_toplevel_set_title_notify;
		wl_signal_add(&surf->toplevel->events.set_title,
			&si_top->set_title);

		wl_list_insert(&srv->toplevels, &si_top->link);
		wlr_xdg_toplevel_set_size(surf,
			srv->viewport_width, srv->viewport_height);
		wlr_xdg_toplevel_set_maximized(surf, 1);
	}
	winlist_close(srv, false);
}

static void surface_destroy_notify(struct wl_listener *listener, void *data)
{
	struct surface_item *si = wl_container_of(listener, si, destroy);
	wl_list_remove(&si->destroy.link);
	wl_list_remove(&si->commit.link);
	wl_list_remove(&si->link);
}

void new_surface_notify(struct wl_listener *listener, void *data)
{
	struct server_data *srv = wl_container_of(listener, srv, new_surface);
	struct wlr_surface *surf = data;
	struct surface_item *si;
	si = calloc(1, sizeof(struct toplevel_item));
	si->server = srv;
	si->surface = surf;
	si->destroy.notify = surface_destroy_notify;
	wl_signal_add(&surf->events.destroy, &si->destroy);
	si->commit.notify = surface_commit_notify;
	wl_signal_add(&surf->events.commit, &si->commit);
	wl_list_insert(&srv->surfaces, &si->link);
}

struct window_manager *init_wm(void)
{
	struct window_manager *wm;

	wm = calloc(1, sizeof(struct window_manager));
	if(!wm) return NULL;

	wm->active = NULL;

	return wm;
}

bool window_is_topbar_hidden(struct toplevel_item *ti)
{
	return ti->fullscreen && !ti->is_topbar_overlapped;
}

bool window_needs_underlay(struct toplevel_item *ti)
{
	return ti->is_alert_screen;
}

static void surface_render_iterator(struct wlr_surface *surf,
	int sx, int sy, void *data)
{
	float matrix[16];
	struct surface_render_data *rd = data;
	struct wlr_box render_box;
	if(!wlr_surface_has_buffer(surf)) return;
	render_box.x = sx * rd->scale + rd->xpos;
	render_box.y = sy * rd->scale + rd->ypos;
	if(!rd->fullscreen) render_box.y += TOPBAR_HEIGHT;
	render_box.width = surf->current.width;
	render_box.height = surf->current.height;
	wlr_matrix_project_box(matrix, &render_box,
		surf->current.transform, 0,
		rd->out->transform_matrix);
	wlr_matrix_scale(matrix, rd->scale, rd->scale);
	wlr_render_texture_with_matrix(rd->renderer,
		wlr_surface_get_texture(surf),
		matrix, 1.0f);
	wlr_surface_send_frame_done(surf, &rd->now);
}

static void window_for_each_surface(struct toplevel_item *ti,
		wlr_surface_iterator_func_t iter,
		void *userdata)
{
	switch(ti->shell){
		case SHELL_XDG:
			wlr_xdg_surface_for_each_surface(
				ti->xdg_surface,
				surface_render_iterator,
				userdata);
			break;
	}
}

void window_render(struct toplevel_item *ti, struct wlr_renderer *renderer,
		struct timespec now, struct wlr_output *out)
{
	struct surface_render_data renderdata = {
		.renderer = renderer,
		.out = out,
		.now = now,
		.scale = 1.0f,
		.xpos = 0,
		.ypos = 0,
		.fullscreen = ti->fullscreen
	};
	window_for_each_surface(ti,
			surface_render_iterator,
			&renderdata);
	if(ti->is_alert_screen){
		wlr_render_texture(renderer,
			ti->title_texture,
			out->transform_matrix,
			ti->server->fullscreen_width * 0.1,
			TOPBAR_HEIGHT, 1.0f);
	}
}

int window_render_side_by_side(struct server_data *srv,
		struct wlr_renderer *renderer, struct timespec now,
		struct wlr_output *out, int spacing,
		int xoffset, int yoffset)
{
	struct toplevel_item *ti;
	struct surface_render_data renderdata = {
		.renderer = renderer,
		.out = out,
		.now = now,
		.scale = WINLIST_WINDOW_SCALE,
		.xpos = xoffset,
		.ypos = yoffset,
		.fullscreen = false
	};
	int counter = 0;
	wl_list_for_each(ti, &srv->toplevels, link){
		if(ti->title_texture){
			wlr_render_texture(renderer,
				ti->title_texture,
				out->transform_matrix,
				renderdata.xpos,
				renderdata.ypos + TOPBAR_HEIGHT -
				ti->title_texture->height,
				1.0f);
		}
		window_for_each_surface(
			ti, surface_render_iterator,
			&renderdata);
		counter++;
		renderdata.xpos += spacing;
	}
	return counter;
}

struct toplevel_item *window_find_by_index(struct server_data *srv, int index)
{
	struct toplevel_item *ti;
	int i = 0;
	wl_list_for_each(ti, &srv->toplevels, link){
		if(i == index) return ti;
		i++;
	}
	return NULL;
}

int window_get_index(struct toplevel_item *ti)
{
	struct server_data *srv = ti->server;
	struct toplevel_item *tmp;
	int i = 0;
	wl_list_for_each(tmp, &srv->toplevels, link){
		if(tmp == ti) return i;
		i++;
	}
	return -1;
}

void window_close(struct toplevel_item *ti)
{
	switch(ti->shell){
		case SHELL_XDG:
			wlr_xdg_toplevel_send_close(ti->xdg_surface);
			break;
	}
}
