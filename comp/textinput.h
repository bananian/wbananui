/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _COMP_TEXTINPUT_H_
#define _COMP_TEXTINPUT_H_

#include <xkbcommon/xkbcommon.h>

struct wlr_text_input_manager_v3;
struct server_data;

struct text_input_info;

struct text_input_info *init_text_input(struct server_data *srv);
void text_input_focus(struct text_input_info *tii, struct wlr_surface *surf);
void text_input_unfocus(struct text_input_info *tii, struct wlr_surface *surf);
int text_input_handle_keypress(struct text_input_info *tii, xkb_keysym_t key);
int text_input_handle_keyrelease(struct text_input_info *tii, xkb_keysym_t key);

#endif
