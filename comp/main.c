/*
 * This file is part of wbananui:
 *  A cairo and Wayland-based user interface for Smart Feature Phones
 * Copyright (C) 2021 Affe Null
 *
 * wbananui is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * wbananui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with wbananui.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/capability.h>
#include <wayland-server.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <pango/pangocairo.h>
#include <wlr/backend.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_screencopy_v1.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/types/wlr_virtual_pointer_v1.h>
#include <wlr/types/wlr_text_input_v3.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/util/log.h>
#include <xkbcommon/xkbcommon.h>
#include <bananui/widget.h>
#include "server.h"
#include "topbar.h"
#include "homescreen.h"
#include "input.h"
#include "output.h"
#include "navigation.h"
#include "wm.h"
#include "textinput.h"
#include "deviceinfo.h"

void ui_sleep(struct server_data *srv)
{
	srv->sleeping = true;
	turn_off_outputs(srv);
	device_suspend(srv->di);
}
void ui_wakeup(struct server_data *srv)
{
	srv->sleeping = false;
	device_resume(srv->di);
	turn_on_outputs(srv);
	device_ensure_awake(srv->di);
}

static int update_clock_timer(void *data)
{
	struct server_data *srv = data;
	time_t curtime;
	struct tm *tm;
	char clockstr[6];
	if(srv->wm->hs_launching_countdown){
		srv->wm->hs_launching_countdown--;
	}
	curtime = time(NULL);
	tm = localtime(&curtime);
	if(0 < strftime(clockstr, sizeof(clockstr), "%H:%M", tm)){
		if(srv->homescreen)
			homescreen_set_clock(srv->homescreen, clockstr);
		if(srv->topbar)
			topbar_set_clock(srv->topbar, clockstr);
	}
	damage_all_outputs(srv);
	wl_event_source_timer_update(srv->clocktimer, 1000);
	return 0;
}

static int capabilities_valid(void)
{
	cap_t pcaps;
	cap_flag_value_t flag;

	pcaps = cap_get_proc();
	if(!pcaps){
		perror("cap_get_proc()");
		return 0;
	}
	if(cap_get_flag(pcaps, CAP_BLOCK_SUSPEND, CAP_EFFECTIVE, &flag)){
		perror("cap_get_flag(CAP_BLOCK_SUSPEND CAP_EFFECTIVE)");
		cap_free(pcaps);
		return 0;
	}
	cap_free(pcaps);
	return flag;
}
#define EVENT_BUFFER_SIZE 2
static void custom_epoll_display_run(struct wl_display *disp,
		struct wl_event_loop *lp)
{
	struct epoll_event ev, events[EVENT_BUFFER_SIZE];
	int fd, epollfd = epoll_create1(EPOLL_CLOEXEC);

	if(epollfd < 0){
		perror("epoll_create");
		return;
	}

	fd = wl_event_loop_get_fd(lp);

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN | EPOLLWAKEUP;
	ev.data.fd = fd;
	if(epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev)){
		perror("epoll_ctl: failed to add event loop");
		return;
	}

	while(1){
		int i;
		int nfds = epoll_wait(epollfd, events, EVENT_BUFFER_SIZE, -1);

		if(nfds < 0 && errno == EINTR) continue;

		if(nfds < 0){
			perror("epoll_wait");
			return;
		}

		for(i = 0; i < nfds; i++){
			if(events[i].data.fd == fd){
				wl_event_loop_dispatch(lp, 0);
				wl_display_flush_clients(disp);
			}
		}
	}
}
int main(int argc, char **argv)
{
	struct server_data srv;
	const char *socket;

	if(!capabilities_valid()){
		fprintf(stderr, "WARNING: CAP_BLOCK_SUSPEND not effective, power management will not work properly\n");
	}

	wlr_log_init(WLR_DEBUG, NULL);
	wl_list_init(&srv.outputs);
	wl_list_init(&srv.inputs);
	wl_list_init(&srv.surfaces);
	wl_list_init(&srv.toplevels);
	srv.sleeping = false;
	srv.homescreen = NULL;
	srv.topbar = NULL;
	srv.wm = init_wm();
	srv.inp = init_input();
	srv.nav = NULL;
	srv.di = device_info_load();
	srv.disp = wl_display_create();
	if(!srv.disp){
		fprintf(stderr, "Failed to create Wayland display\n");
		return 1;
	}

	srv.loop = wl_display_get_event_loop(srv.disp);
	if(!srv.loop){
		fprintf(stderr, "Failed to get event loop\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	srv.backend = wlr_backend_autocreate(srv.disp, NULL);
	if(!srv.backend){
		fprintf(stderr, "Failed to create backend\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	srv.comp = wlr_compositor_create(srv.disp,
		wlr_backend_get_renderer(srv.backend));
	if(!srv.comp){
		fprintf(stderr, "Failed to create compositor\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	wlr_data_device_manager_create(srv.disp);
	wl_display_init_shm(srv.disp);
	wl_display_add_shm_format(srv.disp, WL_SHM_FORMAT_XBGR8888);
	wl_display_add_shm_format(srv.disp, WL_SHM_FORMAT_ABGR8888);

	srv.new_output.notify = new_output_notify;
	wl_signal_add(&srv.backend->events.new_output, &srv.new_output);
	srv.new_input.notify = new_input_notify;
	wl_signal_add(&srv.backend->events.new_input, &srv.new_input);

	socket = wl_display_add_socket_auto(srv.disp);
	if(!socket){
		fprintf(stderr, "Failed to create socket\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	srv.scrcpy = wlr_screencopy_manager_v1_create(srv.disp);
	if(!srv.scrcpy){
		fprintf(stderr, "Failed to create screencopy manager\n");
		wl_display_destroy(srv.disp);
		return 1;
	}
	srv.txtmgr = wlr_text_input_manager_v3_create(srv.disp);
	if(!srv.txtmgr){
		fprintf(stderr, "Failed to create text input manager\n");
		wl_display_destroy(srv.disp);
		return 1;
	}
	srv.txtinp = init_text_input(&srv);
	srv.seat = wlr_seat_create(srv.disp, "Bananui Seat");
	if(!srv.seat){
		fprintf(stderr, "Failed to create seat\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	setenv("WAYLAND_DISPLAY", socket, 1);
	setenv("GDK_BACKEND", "wayland", 1);
	setenv("GTK_IM_MODULE", "wayland", 1);

	if(!wlr_backend_start(srv.backend)){
		fprintf(stderr, "Failed to start backend\n");
		wl_display_destroy(srv.disp);
		return 1;
	}
	srv.shell = wlr_xdg_shell_create(srv.disp);
	if(!srv.shell){
		fprintf(stderr, "Failed to create XDG shell\n");
		wl_display_destroy(srv.disp);
		return 1;
	}

	srv.new_xdg_surface.notify = new_xdg_surface_notify;
	wl_signal_add(&srv.shell->events.new_surface, &srv.new_xdg_surface);

	srv.new_surface.notify = new_surface_notify;
	wl_signal_add(&srv.comp->events.new_surface, &srv.new_surface);

	srv.clocktimer = wl_event_loop_add_timer(srv.loop, update_clock_timer,
		&srv);
	toplevel_activate(&srv, NULL);
	update_clock_timer(&srv);

	custom_epoll_display_run(srv.disp, srv.loop);
	wl_display_destroy(srv.disp);
	return 0;
}
